import React from "react";
import style from "./Course.module.scss";
import { Col, Row, message, Spin } from "antd";
import { getListCourse } from "../../helpers/apis";
import { useEffect } from "react";
import { useState } from "react";
import Course from "./../Course/Course"
import { Link } from "react-router-dom";
function ListCourse() {
  const [listCourse, setListCourse] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [seeMore, setSeeMore] = useState(10)
  useEffect(() => {
    setIsLoading(true)
    getListCourse()
      .then((res) => {
        if (res?.data?.status) {
          setListCourse(res?.data?.data)
        }
      })
      .catch((e) => {
        message.error(e?.data?.message)
      })
      .finally(() => {
        setIsLoading(false)
      })
  }, [])

  return (
    <div className={`${style.Course} py-[30px] sm:py-[50px] text-center bg-[#F8F8F8]`}>
      <div className="container  pb-4">
        <h2 className="text-[#FF5421] mb-7 text-[25px] sm:text-[40px] font-semibold  uppercase ">
          Lựa chọn khóa học
        </h2>
        {
          isLoading
            ?
            <div className="w-full min-h-[70vh] flex items-center justify-center ">
              <Spin tip="Loading..." size="large">
                <div className="content" />
              </Spin>
            </div>
            :
            <>
              <Row gutter={[24, 24]} className="">
                {
                  listCourse.slice(0, seeMore).map((item) => (
                    <Col key={item.id} lg={8} md={12} sm={12} xs={24}>
                      <Course item={item} />
                    </Col>
                  ))
                }

              </Row>
              <button onClick={() => setSeeMore(seeMore + 10)} className="categori_btn inline-block">Xem thêm</button>
            </>
        }

      </div>
    </div>
  );
}

export default ListCourse;
