import React, { useEffect } from "react";
import { Row, Col } from "antd";
import banner from './../HomePage/banner-tiksell-scaled.png'
import { Link } from 'react-router-dom';
function Introduce() {
  useEffect(() => {
    document.title = 'Giới thiệu - TikSell';
  }, []);

  return (
    <div className="text-center mb-40">
      <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[580px] bg-center w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff] bg-no-repeat bg-cover ">
        <div className="bg-[#ffffff99] px-10 py-3">
          <h2 className=" text-[30px] lg:text-[40px] text-[#000] font-bold ">
            Giới thiệu
          </h2>
          <p><Link className='hover:opacity-70 text-[#000] text-[20px] hover:text-[#FF5421] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className="text-[#000] ">Giới thiệu </span> </p>
        </div>
      </div>

      <div className="container mb-7">
        <p className="text-[25px] text-[#ff5421] uppercase mt-4 sm:mt-8">TIKSELL</p>
        <h2 className="text-[25px] sm:text-[30px] font-semibold mt-3">
          Nâng tầm thương hiệu - Đột phá kinh doanh
        </h2>
        <p className="text-[20px] mt-3 text-left">
          Trong thời điểm hiện tại,{" "}
          <span className="font-semibold">Video Marketing </span>là một công cụ
          hoàn hảo để chứng minh sức mạnh của hình ảnh và giúp doanh nghiệp đưa
          sản phẩm ra thị trường.
        </p>
        <p className="text-[20px] mt-3 text-left">
          Cùng với đó, <span className="font-semibold">Tiktok</span> đã và đang
          trở thành một trong những mạng xã hội phổ biến thứ hai tại Việt Nam.
          Điều này cho thấy, truyền thông qua mạng xã hội Tiktok đã là xu hướng
          trong một vài năm qua và khẳng định sẽ tiếp tục phát triển mạnh trong
          những năm tới khi Tiktok toàn cầu sẽ mở thêm nhiều tính năng mới tại
          thị trường Việt Nam.{" "}
        </p>
        <p className="text-[20px] mt-3 text-left">
          Nhận thấy được tiềm năng đó,{" "}
          <span className="font-semibold">Tiksell</span>- công ty có nhiều năm
          kinh nghiệm trong lĩnh vực truyền thông- giải trí, đã đặc biệt quan
          tâm và xây dựng nên bộ giáo trình hoàn chỉnh nhất về cả kiến thức cũng
          như kỹ năng cho lĩnh vực này.{" "}
        </p>
        <p className="text-[20px] mt-3 text-left">
          Với đội ngũ dày dặn kinh nghiệm cùng tiêu chí{" "}
          <span className="font-semibold">
            Nâng tầng thương hiệu- Đột phá doanh thu
          </span>
          , Tiksell là nơi cung cấp các khóa học xây dựng kênh Tiktok nói riêng
          và Video marketing nói chung một cách chuyên nghiệp cho cá nhân và
          doanh nghiệp để học viên có thể dễ dàng chinh phục được xu hướng
          Marketing mới của thời đại 4.0
        </p>
      </div>
      <div className="container ">
        <Row gutter={16} className="justify-center " >
          <Col span={22} lg={6} md={12}>
            <div className="flex justify-center items-center flex-col bg-[#F9F8F8] border-2 rounded-2xl p-8 mt-10  hover:-translate-y-6 duration-500">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/09/3-5.png"
                alt=""
                className="w-2/6 pt-4"
              ></img>
              <p className="text-[25px] font-semibold mt-3">100+</p>
              <p className="text-[20px] text-[#666666] pb-4">Khoá học</p>
            </div>
          </Col>
          <Col span={22} md={12} lg={6} >
            <div className="flex justify-center items-center flex-col bg-[#F9F8F8] border-2 rounded-2xl p-8 mt-10  hover:-translate-y-6 duration-500">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/09/4-2.png"
                alt=""
                className="w-2/6 pt-4"
              ></img>
              <p className="text-[25px] font-semibold mt-3">25K</p>
              <p className="text-[20px] text-[#666666] pb-4">Học viên</p>
            </div>
          </Col>
          <Col span={22} lg={6} md={12}>
            <div className="flex justify-center items-center flex-col bg-[#F9F8F8] border-2 rounded-2xl p-8 mt-10  hover:-translate-y-6 duration-500">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/09/2-7.png"
                alt=""
                className="w-2/6 pt-4"
              ></img>
              <p className="text-[25px] font-semibold mt-3">99 %</p>
              <p className="text-[20px] text-[#666666] pb-4">Đánh giá tốt</p>
            </div>
          </Col>
          <Col span={22} lg={6} md={12}>
            <div className="flex justify-center items-center flex-col bg-[#F9F8F8] border-2 rounded-2xl p-8 mt-10  hover:-translate-y-6 duration-500">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/09/1-6.png"
                alt=""
                className="w-2/6 pt-4"
              ></img>
              <p className="text-[25px] font-semibold mt-3">100 %</p>
              <p className="text-[20px] text-[#666666] pb-4">
                Thực chiến thành công
              </p>
            </div>
          </Col>

        </Row>
      </div>
    </div>
  );
}

export default Introduce;
