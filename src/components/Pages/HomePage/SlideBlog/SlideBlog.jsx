import React, { useState } from "react";
import Slider from 'react-slick';
import BlogItem from "../../../BlogItem/BlogItem";
import style from './SlideBlog.module.scss'
import { useEffect } from "react";
import { postListBlog } from "../../../../helpers/apis";
import { message, Spin } from "antd";
const SlideBlog = () => {
    const [listBlog, setListBlog] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const settings = {
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,

                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    };
    useEffect(() => {
        postListBlog()
            .then((res) => {
                if (res?.data?.status === 200) {
                    setListBlog(res?.data?.data)
                }
                if (res?.data?.status <= 400) {
                    message.error(res?.data?.data?.message)
                }

            }).catch((e) => {
                message.error(e?.response?.data?.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, [])

    return (

        <div className={`pt-[60px] pb-[120px] bg-[#F9F8F8]  ${style.SlideBlog}`}>
            <div className="container">
                <div className="mb-5">

                    <h2 className=" text-[25px] sm:text-[40px] text-[#ff5421] uppercase text-center font-bold ">Tin tức mới nhất</h2>
                </div>
                {
                    isLoading
                        ?
                        <div className="w-full min-h-[30vh] flex items-center justify-center ">
                            <Spin tip="Loading..." size="large">
                                <div className="content" />
                            </Spin>
                        </div>
                        :
                        <Slider {...settings} >

                            {
                                listBlog.map((item, index) => <BlogItem key={item.id} item={item} />)
                            }
                        </Slider>
                }

            </div>
        </div>
    )
}
export default SlideBlog