import React from "react";
// import Banner from "../../Banner/Banner";
import Categories from "../../Categories/Categories";
import ListCourse from "../../ListCourse/ListCourse";

import { useEffect } from 'react';
import InfoTeacher from "./InfoTeacher/InfoTeacher";
import SlideBlog from "./SlideBlog/SlideBlog";
import banner from './banner-tiksell-scaled.png'
import { useDispatch } from "react-redux";
import { setIsOpenRegister } from "../../../Redux/counterSlice";
const HomePage = () => {
    useEffect(() => {
        document.title = 'Trang chủ - TikSell';
    }, []);
    const dispatch = useDispatch()
    return <div >
        <div style={{ backgroundImage: `url(${banner})` }} className={` min-h-[100vh] w-full relative  bg-no-repeat bg-center bg-cover `}>
            <div className=" container h-full flex items-center justify-center flex-col">
                <div className='text-center  mt-[15vh] lg:mt-[30vh] '>
                    <span className='text-[#ff5421] leading-[30px] text-[28px] lg:text-[35px] font-bold '>Khóa Học</span>
                    <h2 className=' max-w-[90%] m-auto lg:max-w-[950px] px-10 py-3 my-5 bg-[#ffffff99]  font-bold text-[20px]  md:text-[30px]  sm:text-[40px]  lg:text-[60px] '>Trở thành chuyên gia TikTok với top 4 khóa học tốt nhất</h2>
                    <div onClick={() => {
                        if (!localStorage.getItem("userName")) {
                            dispatch(setIsOpenRegister(true))
                        }
                    }} className=' hover:bg-[#171f32] cursor-pointer inline-block text-[#fff]  rounded-lg uppercase px-[35px] leading-[40px] py-1 sm:py-2 bg-[#ff5421]  '>Khám phá ngay</div>
                </div>
                <div className="  mt-[5vh] lg:mt-[15vh] max-lg:flex-col flex justify-center gap-[20px] ">
                    <section style={{ transition: "all 500ms ease" }} className=" p-1 lg:p-2.5  hover:translate-y-[-5px] rounded-[5px] min-w-[320px] sm:min-w-[390px] bg-[#000] flex gap-[20px] items-center  ">
                        <img className=" w-[45px] sm:w-[55px]  m-2 sm:m-3 " src="https://keenitsolutions.com/products/wordpress/educavo/wp-content/uploads/2020/10/video-marketing.png" alt="" />
                        <div className="text-[#fff]">
                            <h3 className="font-bold  text-[18px] sm:text-[22px] mb-1">
                                100+ Khóa học online
                            </h3>
                            <span>Khám phá kiến thức mới</span>
                        </div>
                    </section>
                    <section style={{ transition: "all 500ms ease" }} className=" p-1 lg:p-2.5  hover:translate-y-[-5px] rounded-[5px] min-w-[320px] sm:min-w-[390px] bg-[#000] flex gap-[20px] items-center  ">
                        <img className=" w-[45px] sm:w-[55px]  m-2 sm:m-3 " src="https://keenitsolutions.com/products/wordpress/educavo/wp-content/uploads/2020/10/engineering.png" alt="" />
                        <div className="text-[#fff]">
                            <h3 className="font-bold  text-[18px] sm:text-[22px] mb-1">
                                Giảng dạy bởi chuyên gia
                            </h3>
                            <span>Theo sát trong quá trình học</span>
                        </div>
                    </section>
                    <section style={{ transition: "all 500ms ease" }} className=" p-1 lg:p-2.5  hover:translate-y-[-5px] rounded-[5px] min-w-[320px] sm:min-w-[390px] bg-[#000] flex gap-[20px] items-center  ">
                        <img className=" w-[45px] sm:w-[55px]  m-2 sm:m-3 " src="https://keenitsolutions.com/products/wordpress/educavo/wp-content/uploads/2020/10/clock.png" alt="" />
                        <div className="text-[#fff]">
                            <h3 className="font-bold  text-[18px] sm:text-[22px] mb-1">
                                Quyền truy cập trọn đời
                            </h3>
                            <span>Bạn có thể học bất cứ lúc nào</span>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <Categories></Categories>
        <ListCourse></ListCourse>
        <InfoTeacher />
        <SlideBlog />
    </div>
}
export default HomePage