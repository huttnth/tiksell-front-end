
import { Spin, message } from "antd";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { postListBlogDetail } from "../../../helpers/apis";
import banner from './../HomePage/banner-tiksell-scaled.png'
import { formatDateString } from "../../../helpers/common";
import {
    CalendarOutlined,

} from '@ant-design/icons';
const BlogDetail = () => {
    const { id } = useParams();
    const [isLoading, setIsLoading] = useState(false)
    const [blog, setBlog] = useState()
    useEffect(() => {
        setIsLoading(true)
        postListBlogDetail({ id: id })
            .then((res) => {
                if (res?.data?.status === 200) {
                    setBlog(res?.data?.data)
                }
                if (res?.data?.status <= 400) {
                    message.error(res?.data?.data?.message)
                }

            })
            .catch((e) => {
                message.error(e?.data?.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, [id])

    return (
        <div>
            <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[580px]  w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff] bg-center  bg-no-repeat  bg-cover  ">
                <div className="bg-[#ffffff99] px-10 py-3">
                    <h2 className=" text-[25px] text-center  lg:text-[40px] font-bold text-[#000] ">
                        Trang tin tức
                    </h2>
                    <p><Link className='hover:opacity-70 text-[#000] text-[20px] hover:text-[#FF5421] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className="text-[#000] ">Trang chi tiet tin tức</span> </p>
                </div>
            </div>
            <div className="mt-[50px] mb-[200px] ">
                <div className="container ] ">
                    <div className="flex w-full">

                        {
                            isLoading
                                ?

                                <div className="w-full min-h-[70vh] flex items-center justify-center ">
                                    <Spin tip="Loading..." size="large">
                                        <div className="content" />
                                    </Spin>
                                </div>
                                :
                                <div>
                                    <h2 className=" text-[30px] font-bold lg:text-[40px]">{blog?.title}</h2>
                                    <span className="inline-block my-3"><CalendarOutlined className="mr-2 text-[#ff5421] " />{formatDateString(blog?.updated_at)}</span>

                                    <div dangerouslySetInnerHTML={{ __html: blog?.content.replace(/"/g, "") }} style={{

                                    }}></div>


                                </div>

                        }

                    </div>
                </div>
            </div>

        </div>
    )
}
export default BlogDetail