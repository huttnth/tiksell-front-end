import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import banner from './../HomePage/banner-tiksell-scaled.png'
import { postListBlog } from '../../../helpers/apis';
import BlogItem from '../../BlogItem/BlogItem';
import { message } from 'antd';
import { Row, Col, Spin } from "antd";
const BlogPage = () => {
    const [listBlog, setListBlog] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
        document.title = 'Tin Tức - TikSell';
        postListBlog()
            .then((res) => {
                if (res?.data?.status === 200) {
                    setListBlog(res?.data?.data)

                }
                if (res?.data?.status <= 400) {
                    message.error(res?.data?.data?.message)
                }

            }).catch((e) => {
                message.error(e?.response?.data?.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, []);

    return <div>

        <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[380px] lg:min-h-[580px]  w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff]  bg-no-repeat  bg-cover bg-center ">
            <div className=' bg-[#ffffff99] px-10 py-3 text-center'>
                <h2 className=' text-[25px] sm:text-[40px] font-bold mb-[5px] text-[#000]  '>Tin Tức</h2>
                <p><Link className='hover:opacity-70 text-[20px]  text-[#000] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className='text-[#000]'>Tin tức</span> </p>
            </div>
        </div>
        <div className=' mt-16'>
            <div className='container'>
                {
                    isLoading
                        ?

                        <div className="w-full min-h-[70vh] flex items-center justify-center ">
                            <Spin tip="Loading..." size="large">
                                <div className="content" />
                            </Spin>
                        </div>
                        :

                        <Row gutter={[24, 24]} className="mb-40">

                            {

                                listBlog.map((item) => {
                                    return (
                                        <Col lg={8} md={12} sm={12} xs={24}>
                                            <BlogItem key={item.id} item={item} />
                                        </Col>
                                    )
                                })
                            }
                        </Row>


                }
            </div>
        </div>

    </div>
}
export default BlogPage