import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import { Col, Row, message, Spin } from "antd";
import style from "./../../Course/Course.module.scss"
import { getListCourse } from "../../../helpers/apis";
import Course from "../../Course/Course";
import banner from './../HomePage/banner-tiksell-scaled.png'
const CoursesPage = () => {
    const [listCourse, setListCourse] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
        document.title = 'Khóa học - TikSell';
        setIsLoading(true)
        getListCourse()
            .then((res) => {
                if (res?.data?.status) {
                    setListCourse(res?.data?.data)
                }
            })
            .catch((e) => {
                message.error(e?.data?.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, [])
    const rate = 3;
    return <div className={`${style.Course} `}>
        <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[380px] lg:min-h-[580px]    w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff] bg-center  bg-no-repeat  bg-cover  ">
            <div className="bg-[#ffffff99] px-10 py-3 text-center">
                <h2 className=' text-[30px]  lg:text-[40px] font-bold mb-[5px] text-[#000]  '>Khóa Học</h2>
                <p><Link className='hover:opacity-70 text-[#000] text-[20px] hover:text-[#FF5421] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className="text-[#000] ">Khóa Học</span> </p>
            </div>
        </div>
        <div className="container ">
            <div className="mt-[50px] ">
                {
                    isLoading
                        ?
                        <div className="w-full min-h-[70vh] flex items-center justify-center ">
                            <Spin tip="Loading..." size="large">
                                <div className="content" />
                            </Spin>
                        </div>
                        :
                        <div>
                            <h3 className="text-center text-[30px] mb-10">Danh sách khóa học</h3>
                            <Row gutter={[24, 24]} className="mb-40 ">

                                {

                                    listCourse.map((item) => (
                                        <Col key={item.id} lg={8} md={12} sm={12} xs={24}>
                                            <Course item={item} />
                                        </Col>
                                    ))
                                }
                            </Row>
                        </div>
                }

            </div>
        </div>
    </div>
}
export default CoursesPage