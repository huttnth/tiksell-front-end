import React, { useEffect, useState } from 'react'
import { getMyCourse, getTransactionHistory } from '../../../helpers/apis'
import { message, Spin, Col, Row, Table } from 'antd'
import Course from "../../Course/Course"
import { formatNumber } from '../../../helpers/common'

const Profile = () => {
    const [isActive, setIsActive] = useState(1)
    const [listCourse, setListCourse] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [TransactionHistory, setTransactionHistory] = useState([])
    useEffect(() => {
        setIsLoading(true)
        getMyCourse()
            .then((res) => {
                if (res?.data?.status) {
                    setListCourse(res?.data?.data)
                }
            })
            .catch((e) => {
                message.error(e?.data?.message)
            })
            .finally(() => {
                setIsLoading(false)
            })

    }, [])
    useEffect(() => {
        setIsLoading(true)
        getTransactionHistory()
            .then((res) => {
                if (res.data.status >= 200) {
                    setTransactionHistory(res.data.data)
                }
                if (res.data.status >= 400) {
                    message.error(res.data.message)
                }

            }).catch((e) => {

                message.error(e.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, [isActive === 3])
    const columns = [
        {
            title: 'Khóa học',
            key: 1,
            dataIndex: 'course',
            width: 200,
            render: (value) => value.title
        },
        {
            title: 'Giá tiền ',
            ellipsis: true,
            key: 2,
            width: 200,
            dataIndex: 'amount',
            render: (value) => formatNumber(value)
        },
        {
            title: 'Thời gian tạo',
            ellipsis: true,
            key: 3,
            width: 200,
            dataIndex: 'created_at',

        },
        {
            title: 'Trạng thái thanh toán',
            ellipsis: true,
            key: 4,
            width: 200,
            dataIndex: 'status',
            render: (value) => {
                return <div>{value === 0 && "Đã thanh toán" || value === 3 && "Chờ Xác nhận" || value === 4 && "Chờ thanh toán " || value === 1 && "Đã thanh toán"}</div>
            }
        },

    ]
    return (
        <div className='min-h-[50vh] mb-52 lg:mb-36  '>
            <div className='container '>
                <div className='w-full h-full min-h-[50vh]  py-8 mt-10 px-3 md:p-10 rounded-lg shadow-lg border border-[#ccc]'>
                    <div className='bg-[#fff]  '>
                        <div className='flex gap-3 border-b pb-5 border-[#ccc] '>

                            <button onClick={() => setIsActive(1)} className={`px-2 py-1 border border-[#ccc] rounded-md ${isActive === 1 && "bg-[red] text-[#fff] "}`} >Khóa học</button>
                            <button onClick={() => setIsActive(2)} className={`px-2 py-1 border border-[#ccc] rounded-md ${isActive === 2 && "bg-[red] text-[#fff] "}`} >Lịch sử giao dịch</button>
                        </div>
                        <div className='w-full'>

                            {
                                isActive === 1
                                &&
                                <div className='flex'>

                                    {
                                        isLoading
                                            ?
                                            <div className="w-full min-h-[70vh] flex items-center justify-center ">
                                                <Spin tip="Loading..." size="large">
                                                    <div className="content" />
                                                </Spin>
                                            </div>
                                            :
                                            <Row gutter={[24, 24]} className=" mt-7 w-full">
                                                {
                                                    listCourse.map((item) => (
                                                        <Col key={item?.id} lg={8} md={12} sm={12} xs={24}>

                                                            <Course item={item} />
                                                        </Col>
                                                    ))
                                                }

                                            </Row>
                                    }

                                </div>
                            }
                            {
                                isActive === 2
                                &&
                                <div className='flex'>

                                    {
                                        isLoading
                                            ?
                                            <div className="w-full min-h-[70vh] flex items-center justify-center ">
                                                <Spin tip="Loading..." size="large">
                                                    <div className="content" />
                                                </Spin>
                                            </div>
                                            :
                                            <Table
                                                scroll={{ x: 1000 }}
                                                bordered columns={columns} dataSource={TransactionHistory.map((item, index) => {
                                                    return { ...item, key: index + 1 }
                                                })} />

                                    }
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Profile