import React, { useState } from "react";
import style from './VideosTeaching.module.scss'
import { YoutubeFilled, LockOutlined, UnlockOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';
import PlayVideo from "../../../PlayVideo/PlayVideo";
const VideosTeaching = ({ item, status }) => {
    const [isShowVideo, setIsShowVideo] = useState(false)
    const [urlVideo, setUrlVideo] = useState()
    return (
        <div className={style.container}>
            <div className=" border border-[#e0e2ea] rounded-md " >
                <div >
                    <div className="   cursor-pointer border-b border-[#e0e2ea] " onClick={() => setIsShowVideo(!isShowVideo)} >
                        <div className=" py-4 px-4 flex items-center justify-between ">
                            <h4 className={`${isShowVideo ? "text-[#3e64de]" : "text-[#000]"} rounded-tl-6 rounded-tr-6 text-[20px]  font-medium `}>
                                {item?.name}
                            </h4>
                            <span>
                                {
                                    isShowVideo
                                        ?
                                        <DownOutlined />
                                        :
                                        <UpOutlined />
                                }
                            </span>
                        </div>
                    </div>
                    {
                        isShowVideo
                        &&
                        <div className={`transition-all duration-500 ease-in-out `}>
                            <ul>
                                {
                                    item?.category_items?.map((item2) => (
                                        <li key={item2.id} style={{ lineHeight: "1.6" }} onClick={() => setUrlVideo((prev) => {
                                            if (prev === item2.path_name) {
                                                return ""
                                            } else {
                                                return item2.path_name
                                            }
                                        })} className=" cursor-pointer py-2 px-4 transition duration-300 ease-in  hover:bg-[#eff1f6]  ">
                                            <div className="flex justify-between">
                                                <div className="flex gap-3 items-center">
                                                    <YoutubeFilled className="" />
                                                    <p className=" max-w-[100px] min-[350px]:max-w-[130px] min-w-[400px]:max-w-[200px]  sm:max-w-[300px] lg:max-w-[550px] truncate"> {item2.name}</p>
                                                </div>
                                                <div >
                                                    <span className="mr-2.5">00:00</span>
                                                    <span>
                                                        {status === 1 ? <UnlockOutlined /> : <LockOutlined />}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="teaching_video">
                                                {urlVideo === item2.path_name && status === 1 && <PlayVideo url={urlVideo} />}
                                            </div>
                                        </li>
                                    ))
                                }

                            </ul>
                        </div>
                    }
                </div>

            </div >
        </div>
    )
}
export default VideosTeaching