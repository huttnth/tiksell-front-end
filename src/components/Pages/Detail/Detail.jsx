import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Row, Col, Spin, Modal, Input, Button, message, Image } from "antd";
import {
  StarOutlined,
  StarFilled,
  WarningOutlined,
  GlobalOutlined,
  CheckOutlined,
} from "@ant-design/icons";
import VideosTeaching from "./VideosTeaching/VideosTeaching";
import { postDetail, postPayment } from "../../../helpers/apis";
import { formatNumber } from "../../../helpers/common";
import banner from './../HomePage/banner-tiksell-scaled.png'
import { Link } from 'react-router-dom';
import PlayVideo from "../../PlayVideo/PlayVideo";
function Detail() {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingPayment, setIsLoadingPayment] = useState(false)
  const [isOpenPayment, setIsOpenPayment] = useState(false)
  const [inputPayment, setInputPayment] = useState()
  const handleChangInputPayment = (value) => {
    setInputPayment({
      ...inputPayment,
      [value.target.name]: value.target.value
    })
  }
  const [infoCourse, setInfoCourse] = useState()
  const postDetail1 = () => {
    postDetail({ slug: id })
      .then((res) => {
        if (res.data.status === 200) {
          document.title = `Khóa học - ${res.data.data.title}`
          setIsLoading(false)
          setInfoCourse(res.data.data)
        }
      })
      .catch((e) => {
        console.log(e)
      })
  }
  useEffect(() => {
    setIsLoading(true)
    postDetail1()
  }, [])
  const handleSendPayment = () => {
    const input = {
      course_id: infoCourse.id,
      phone_number: localStorage.getItem("phoneNumber") || inputPayment?.phoneNumber,
      name: localStorage.getItem("userName") || inputPayment?.fullName,
      transfer_content: `${localStorage.getItem("userName") || inputPayment?.fullName} ${localStorage.getItem("phoneNumber") || inputPayment.phoneNumber} `
    }
    setIsLoadingPayment(true)
    postPayment(input)

      .then((res) => {
        if (res.data.status === 200) {
          message.success(res.data.message)
          postDetail1()
        }
        if (res.data.status >= 400) {
          message.error(res.data.message)
        }
      })
      .catch((e) => {
        console.log(e)
      })
      .finally(() => {
        setIsLoadingPayment(false)
      })

  }
  const date = new Date(infoCourse?.created_at)
  return (
    <div>
      <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[580px]  w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff] bg-center  bg-no-repeat  bg-cover  ">
        <div className="bg-[#ffffff99] px-10 py-3">
          <h2 className=" text-[25px] text-center  lg:text-[40px] font-bold text-[#000] ">
            {infoCourse?.title}

          </h2>
          <p><Link className='hover:opacity-70 text-[#000] text-[20px] hover:text-[#FF5421] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className="text-[#000] ">Trang chi tiêt sản phẩm</span> </p>
        </div>
      </div>
      <Row className="bg-[black] text-[white] pb-6 ">
        <Col className="container">
          <h3 className=" text-[25px] lg:text-[35px] font-semibold mt-8">
            {infoCourse?.title}
          </h3>
          <p className=" text-[16px] lg:text-[20px] font-medium mt-4">
            {infoCourse?.description}
          </p>
          <div className="mt-4 pr-4 mb-4">
            {Array.from({ length: infoCourse?.rate_number }, (_, index) => (
              <StarFilled
                key={index}
                style={{
                  color: "#FFD700",
                  fontSize: "18px",
                  marginRight: "5px",
                }}
              />
            ))}
            {Array.from({ length: 5 - infoCourse?.rate_number }, (_, index) => (
              <StarOutlined
                key={index + infoCourse?.rate_number}
                style={{
                  color: "lightgray",
                  fontSize: "18px",
                  marginRight: "5px",
                }}
              />
            ))}
            <span className="  mr-3 font-semibold">({infoCourse?.rate_people} Đánh giá)</span>
            <span>1000 học viên</span>
          </div>
          <div className="flex justify-start items-center mb-5">
            <div className="mr-8">
              <WarningOutlined></WarningOutlined>
              <span className="ml-2"> Lần cập nhật gần đây nhất {date.getDate()}/{date.getMonth() + 1}/{date.getFullYear()}</span>
            </div>
            <div>
              <GlobalOutlined></GlobalOutlined>
              <span className="ml-2"> Vietnamese</span>
            </div>
          </div>
        </Col>
      </Row>
      {
        isLoading
          ?
          <div className="w-full min-h-[70vh] flex items-center justify-center ">
            <Spin tip="Loading..." size="large">
              <div className="content" />
            </Spin>
          </div>
          :
          <div className="container">
            <Row className=" text-[#2d2f31] flex gap-5  max-md:flex-col-reverse max-md:gap-10 pb-6 mb-60 mt-10">
              <Col
                lg={17}
                md={15}
                sm={24}
                xs={24}
                className="p-4 max-sm:mt-[50px] max-[750px]:mt-[100px] border-2 border-[white] rounded"
                style={{ boxShadow: "0 0 30px rgba(0,0,0,.3)" }}
              >

                <Row gutter={[16, 16]}>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-2"></CheckOutlined>
                    <span className="text-justify">
                      Nhận diện và phát huy những điểm mạnh của mình: Xác định giá
                      trị cốt lõi và sứ mệnh của bản thân;
                    </span>
                  </Col>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-1"></CheckOutlined>
                    <span className="text-justify">
                      Xây dựng đúng và đủ về hình ảnh của mình trong mắt bạn bè,
                      đồng nghiệp, xã hội, đặc biệt là nhà tuyển dụng tiềm năng;
                    </span>
                  </Col>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-1"></CheckOutlined>
                    <span className="text-justify">
                      Phát huy hết những tiềm năng của mình: Kết nối với những người
                      có cùng giá trị và mục tiêu;
                    </span>
                  </Col>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-1"></CheckOutlined>
                    <span className="text-justify">
                      Xây dựng đúng và đủ về hình ảnh của mình trong mắt bạn bè,
                      đồng nghiệp, xã hội, đặc biệt là nhà tuyển dụng tiềm năng;
                    </span>
                  </Col>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-1"></CheckOutlined>
                    <span className="text-justify">
                      Là chính mình và được mọi người nhìn nhận đúng để yêu quý vì
                      chính bạn;
                    </span>
                  </Col>
                  <Col lg={12} className="mb-1 flex justify-start items-start">
                    <CheckOutlined className="mr-3 mt-1"></CheckOutlined>
                    <span className="text-justify">
                      Sử dụng các công cụ marketing để giới thiệu mình hiệu quả với
                      thế giới.
                    </span>
                  </Col>
                </Row>
                <Row>
                </Row>
                <div className="mt-[50px] w-full flex flex-col gap-5">
                  {
                    infoCourse?.categories?.map((item) => <VideosTeaching key={item.id} status={infoCourse?.is_active} item={item} />)
                  }

                </div>
              </Col>
              <Col lg={6} md={8} sm={24} xs={24} className="lg:pl-4">
                {
                  infoCourse?.video_thumbnail
                    ?
                    <div className="  w-full  video_trailer">
                      <PlayVideo width="100%" hight="300px" url={infoCourse?.video_thumbnail} />
                    </div>
                    :
                    <Image

                      src={`${infoCourse?.thumbnail_image || "error"}`}
                      alt=""
                      fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                      className="w-full min-h-[350px] max-h-[350px]   object-contain course_hover-img relative"
                    ></Image>
                }

                <div
                  className="bg-[white] text-[black] rounded pt-2 pr-3 pl-3"
                  style={{ boxShadow: "0 0 30px rgba(0,0,0,.3)" }}
                >
                  <div>

                    <span className="text-[24px] font-semibold"> Giá : {formatNumber(infoCourse?.price)}</span>
                  </div>
                  <div className="mt-4">

                    {
                      infoCourse?.is_active === 1
                        ?
                        <button className="w-full text-[16px] bg-[#FF5421] border-2 text-[#fff]  border-[#bfbcbc] font-semibold pt-2 pb-2 transition ease-in-out duration-1000">
                          Đã mua
                        </button>
                        :
                        <button onClick={() => setIsOpenPayment(true)} className="w-full text-[16px]  border-2  border-[#bfbcbc] font-semibold pt-2 pb-2 bg-[#FF5421] text-[white] transition ease-in-out duration-1000">
                          Mua ngay
                        </button>
                    }
                  </div>
                  <div>
                    <p className="text-center text-[#b8b7b7] text-[13px] mt-3">
                      Đảm bảo hoàn tiền trong 30 ngày
                    </p>
                  </div>
                  <div className="bg-white p-4 ">
                    <span className="text-lg font-semibold">
                      Khóa học này bao gồm:
                    </span>
                    <ul className="list-disc ml-6 mt-2">
                      <li>2 giờ video theo yêu cầu</li>
                      <li>Truy cập trên thiết bị di động và TV</li>
                      <li>Quyền truy cập đầy đủ suốt đời</li>
                      <li>Giấy chứng nhận hoàn thành</li>
                    </ul>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
      }
      <Modal
        title={<div className="text-center text-[24px] font-bold mb-7 ">Thanh toán khóa học</div>}
        centered
        open={isOpenPayment}
        onOk={() => setIsOpenPayment(false)}
        onCancel={() => setIsOpenPayment(false)}
        width={750}
        footer={false}
      >
        <div className="flex max-lg:flex-col gap-3">
          <div className="flex flex-col gap-4">
            <p><span className="font-bold">Tên tài khoản  :</span> DO DAO NHAT DUONG</p>
            <p> <span className="font-bold">Số tài khoản: </span> 9235888888</p>
            <p><span className="font-bold">(*) Lưu ý : </span> Vui lòng bấm xác nhận thanh toán để được duyệt  </p>
            {
              !localStorage.getItem("userName")
              &&
              <>
                <Input rules={[{ required: true, message: 'Vui lòng nhập thông tin của bạn!' }]} name="fullName" className="text-[18px]" placeholder="Họ và tên" onChange={handleChangInputPayment} />
                <Input rules={[{ required: true, message: 'Vui lòng nhập Số điện thoại của bạn của bạn!' }]} name="phoneNumber" className="  text-[18px]" placeholder="Số điện thoại" onChange={handleChangInputPayment} />
              </>
            }

          </div>
          <div className="w-full lg:w-6/12">
            <img src={`https://img.vietqr.io/image/ACB-9235888888-compact.png?amount=${infoCourse?.price}&addInfo=${`${localStorage.getItem("userName") || inputPayment?.fullName} ${localStorage.getItem("phoneNumber") || inputPayment?.phoneNumber} `}`} alt="" />

          </div>

        </div>
        <Button disabled={infoCourse?.is_waiting >= 1 ? true : false} loading={isLoadingPayment} onClick={handleSendPayment} className="mt-3 block mx-auto bg-blue-600 text-[#fff] h-10   ">Xác nhận thanh toán</Button>
      </Modal>
    </div>
  );
}

export default Detail;
