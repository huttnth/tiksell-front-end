import React from "react";
import { Row, Col } from "antd";
import banner from './../HomePage/banner-tiksell-scaled.png'
import { Link } from 'react-router-dom';
function Contact() {
  return (
    <div>
      <div style={{ backgroundImage: `url(${banner})` }} className="min-h-[100vh] bg-center  w-full relative flex flex-col items-center justify-center py-[100px] text-[#fff]  bg-no-repeat  bg-cover  ">
        <div className="bg-[#ffffff99] px-10 py-3">
          <h2 className=" text-[50px] font-bold text-[#000] ">
            Liên hệ
            <p><Link className='hover:opacity-70 text-[#000] text-[20px] hover:text-[#FF5421] ' to={"/"}>Trang chủ</Link> <span className='mx-2 text-[#000]'>/</span> <span className="text-[#000] ">Liên hệ</span> </p>
          </h2>
        </div>
      </div>

      <div className="mb-40 mt-40 container">
        <h3 className="text-center text-[30px] lg:text-[40px] text-[#FF5421] mt-8">Liên Hệ</h3>
        <Row gutter={[16, 16]} className="flex  mt-10 justify-between">
          <Col
            span={24}
            lg={7}
            className="bg-[#e4dcdc] pt-6 rounded-xl"
            style={{ boxShadow: "0 0 30px rgba(0,0,0,.3)" }}
          >
            <div className="flex justify-center items-center flex-col">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/10/address.png"
                alt=""
                className="w-1/6 text-[#FF5421]"
              />

              <p className="text-[20px] mt-4 font-semibold">Địa Chỉ</p>
              <p className="text-[16px] mt-4 pl-6 pr-6 pb-6 font-semibold">
                Số 5, LK6B, C17 Bộ Công An, Đ. Nguyễn Văn Lộc, P. Mỗ Lao, Q.Hà
                Đông, Hà Nộ
              </p>
            </div>
          </Col>
          <Col
            span={24}
            lg={7}
            className="bg-[#e4dcdc] pt-6 rounded-xl"
            style={{ boxShadow: "0 0 30px rgba(0,0,0,.3)" }}
          >
            <div className="flex justify-center items-center flex-col">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/10/open-email.png"
                alt=""
                className="w-1/6 text-[#FF5421]"
              />

              <p className="text-[20px] mt-4 font-semibold">Email</p>
              <p className="text-[16px] mt-4 pl-6 pr-6 pb-6 font-semibold">
                partner@tiksell.vn
              </p>
            </div>
          </Col>
          <Col
            span={24}
            lg={7}
            className="bg-[#e4dcdc] pt-6 rounded-xl"
            style={{ boxShadow: "0 0 30px rgba(0,0,0,.3)" }}
          >
            <div className="flex justify-center items-center flex-col">
              <img
                src="https://keenitsolutions.com/products/wordpress/educavo-tutor/wp-content/uploads/2020/10/smartphone.png"
                alt=""
                className="w-1/6 text-[#FF5421]"
              />

              <p className="text-[20px] mt-4 font-semibold">Hotline + Zalo  </p>
              <p className="text-[16px] mt-4 pl-6 pr-6 pb-6 font-semibold">
                0972350669 - 0985072175
              </p>
            </div>
          </Col>
        </Row>
        <Row className="mb-40 mt-20 flex justify-center gap-4 items-center">
          <Col lg={12}>
            <iframe
              className="w-[600px] h-[500px]"
              title="bando"
              src="https://maps.google.com/maps?width=600&amp;height=400&amp;hl=en&amp;q=Luxury, Liền kề Kiến Hưng, Hà Đông, Hà Nội 100000&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
            ></iframe>
          </Col>
          <Col lg={12}>
            <p className="text-[20px] pl-10 ">
              Đăng ký nhận tư vấn Bạn đang gặp vấn đề hoặc cần tư vấn thêm về
              khóa học? hãy liên hệ ngay với chúng tôi để được Support 24/7.
            </p>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Contact;
