import React from "react";
import { Spin } from 'antd';
const Loading = () => {
    return (
        <Spin className="w-full h-full" size="large" />
    )
}
export default Loading