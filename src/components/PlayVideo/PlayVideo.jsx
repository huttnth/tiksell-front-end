import React from "react";

const PlayVideo = ({ url, width, hight }) => {

    return (

        <div className={`${width ? `max-w-[${width}] max-h-[${hight}] min-w-[${width}] min-h-[${hight}] ` : "w-full h-full"} min-w-full`} >
            <div className="w-full " dangerouslySetInnerHTML={{ __html: url }} />
        </div>

    )
}
export default PlayVideo