import React from "react";
import style from './BlogItem.module.scss'
import {
    CalendarOutlined,
    UserOutlined
} from '@ant-design/icons';
import { Link, useParams } from "react-router-dom";
import { formatDateString } from "../../helpers/common";
const BlogItem = ({ item }) => {


    return (
        <div className={`${style.blogItem} mx-2`}>
            <Link to={`/chi-tiet-tin-tuc/${item?.id}`}>

                <div className="flex flex-col  justify-start items-start cursor-pointer bg-[white] blog_item ">
                    <div className="blog_content-img relative w-full">
                        <img
                            src={item?.thumbnail}
                            alt=""
                            className=" max-h-[300px] min-h-[300px] blog_hover-img relative object-fill"
                        ></img>

                    </div>

                    <div className=" p-[30px] w-full  ">

                        <ul className="flex justify-between px-7 ">
                            <li><CalendarOutlined className="mr-2 text-[#ff5421] " />{formatDateString(item?.created_at)}</li>
                            <li><UserOutlined className="mr-2 text-[#ff5421] " /> admin</li>
                        </ul>
                        <h3 className="hover:text-[#ff5421] text-left my-5 font-bold max-h-[50px] min-h-[50px] line-clamp-3  ">
                            {item?.title}
                        </h3>

                    </div>
                </div>
            </Link>
        </div>
    )
}
export default BlogItem