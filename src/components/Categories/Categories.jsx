import React from "react";
import style from "./Categories.module.scss";
import { Col, Row } from "antd";
import Slider from "react-slick";
function Categories() {
  const dataCategories = [
    {
      id: 111,
      title: "TikTok Shop Master",
      listContent: [
        {
          id: 1,
          title: "Cách xây dựng trận đại bán hàng"
        },
        {
          id: 2,
          title: "Bùng nổ doanh số với  các hoạt động  tiếp thị quảng cáo"
        },
        {
          id: 3,
          title: "Bí quyết chốt đơn trên livestream"
        },
        {
          id: 4,
          title: "Công thức lên chiến dịch quảng cáo hiệu quả"
        },
      ]
    },
    {
      id: 222,
      title: "TikTok Shop Basic",
      listContent: [
        {
          id: 5,
          title: "Tìm hiểu TTs & Khởi tạo gian hàng"
        },
        {
          id: 6,
          title: "Chính sách Livestream bán hàng"
        },
        {
          id: 7,
          title: "Khuyến mại , chiến dịch và công cụ quảng bá"
        },
        {
          id: 8,
          title: "Sepup , do lường tối ưu chiến dịch quảng cáo"
        },
      ]
    },
    {
      id: 333,
      title: "TikTok Shop Basic",
      listContent: [
        {
          id: 9,
          title: "Duy trì hiệu suất quảng cáo"
        },
        {
          id: 10,
          title: "Kiểm soát ngân sách"
        },
        {
          id: 11,
          title: "Tối ưu chi phí"
        },
        {
          id: 12,
          title: "Scale campaign"
        },
      ]
    },
    {
      id: 444,
      title: "Coaching 1-1",
      listContent: [
        {
          id: 13,
          title: "Tư vấn chọn sản phẩm"
        },
        {
          id: 14,
          title: "Các giải pháp triển khai trên TikTok"
        },
        {
          id: 15,
          title: "Quy trình triển khai và vận hành"
        },
        {
          id: 16,
          title: "Đòng hành cùng Brand"
        },
      ]
    },
    {
      id: 555,
      title: "Coaching 1-2",
      listContent: [
        {
          id: 17,
          title: "Tư vấn chọn sản phẩm"
        },
        {
          id: 18,
          title: "Các giải pháp triển khai trên TikTok"
        },
        {
          id: 19,
          title: "Quy trình triển khai và vận hành"
        },
        {
          id: 20,
          title: "Đòng hành cùng Brand"
        },
      ]
    }
    ,
    {
      id: 666,
      title: "Coaching hệ thống doanh nghiệp",
      listContent: [
        {
          id: 17,
          title: "Tư vấn chọn chiến lược"
        },
        {
          id: 18,
          title: "giải pháp cho doanh nghiệp"
        },
        {
          id: 19,
          title: "Nâng tầm branding"
        },
        {
          id: 20,
          title: "Quy trinh tikTok Ads cho doanh nghiệp"
        },
      ]
    }
  ]
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <div className={`${style.Categories}  text-center py-[60px]`} >
      <div className="container">
        <div className=" mb-10">
          <h2 className="text-[#FF5421] text-[25px]  lg:text-[40px] mb-3 font-semibold uppercase">
            Chương Trình Đào Tạo
          </h2>
        </div>

        <Slider className="flex gap-5" {...settings}>
          {
            dataCategories.map((item) => (
              <div key={item.id} className=" categori_product  ">
                <div
                  className="p-4 text-left mx-3 rounded  min-h-[400px]  flex flex-col justify-around category_img"

                >
                  <div className="z-20">
                    <h3 className=" text-[22px] text-center font-bold  text-[white]">
                      {item.title}
                    </h3>
                    <ul className="ml-6 mt-2 list-inside space-y-2">
                      {
                        item.listContent.map(item2 => (
                          <li key={item2.id} className="text-left flex items-center ">
                            <span className="text-[#03d9eb] text-[30px] mr-3">•</span>
                            <span className="text-[white] font-semibold">
                              {item2.title}
                            </span>
                          </li>
                        ))
                      }

                    </ul>
                  </div>
                  <a href="https://zalo.me/0985072175" className="categori_btn cursor-pointer z-10">Liên hệ để được tư vấn</a>
                </div>
              </div>
            ))
          }
        </Slider>



      </div>
    </div >
  );
}

export default Categories;
