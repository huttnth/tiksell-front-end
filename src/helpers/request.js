import axios from "axios";

export const instanceAxios = axios.create({
    baseURL: 'https://demo.tiksell.vn/api/',
    headers: { 'X-Custom-Header': 'foobar' }
});
instanceAxios.interceptors.request.use(async (config) => {
    const token = localStorage.getItem("tokenUser") ?? ''


    if (token) {

        config.headers = {
            Authorization: `Bearer ${token}`,
            Accept: 'application/json , application/msn.api.v1+json',
        }

    } else {
        config.headers = {
            Accept: 'application/json, application/msn.api.v1+json',
        }
    }


    return config;
})