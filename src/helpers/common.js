export const formatNumber = (num, unit = 'đ') => num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " " + unit
export const formatDateString = (dateString) => {
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString('en-US');
    return formattedDate;
}