import { instanceAxios } from "./request";


export const getallCategory = () => instanceAxios.post(`/admin/type/list
`)
export const editCategory = (data) => instanceAxios.post(`/admin/type/update
`, data)
export const deleteCategory = (id) => instanceAxios.post(`/admin/type/delete`, id)
export const createCategory = (data) => instanceAxios.post(`/admin/type/create
`, data)
export const apiAdmin = () => instanceAxios.post(`/admin/dashboard
`,)

export const deleteUser = (users) => instanceAxios.post("/admin/user/delete/batch", users)
export const postReceipt = () => instanceAxios.post("/admin/receipt/list")
export const postUpdateReceipt = (receipt) => instanceAxios.post("/admin/receipt/update", receipt)

export const GetAllUser = () => instanceAxios.post("/admin/user/list")
export const updateUser = (user) => instanceAxios.post("/admin/user/update", user)
export const adminProduct = () => instanceAxios.post("/admin/course/list")
export const adminCategory_product = () => instanceAxios.post("/admin/type/list")
export const createProduct_category = (data) => instanceAxios.post("/admin/course/create")
export const admin_blogs = (data) => instanceAxios.post("/admin/blog/list")
export const admin_createBlog = (data) => instanceAxios.post("/admin/blog/create", data)

export const delete_Blog = (id) => instanceAxios.post("/admin/blog/delete", id)

export const getoneBlog = (id) => instanceAxios.post("/blog/detail", id)
export const updateBlog = (data) => instanceAxios.post('/admin/blog/update', data)
export const prdocuctAdmin = (data) => instanceAxios.post('/admin/course/create', data)


