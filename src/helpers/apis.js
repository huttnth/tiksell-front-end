import { instanceAxios } from "./request";

export const postRegister = (user) => instanceAxios.post(`/register`, user)
export const postLogin = (user) => instanceAxios.post(`/login`, user)
export const postDetail = (id) => instanceAxios.post(`/course/detail`, id)
export const postLogout = () => instanceAxios.post(`/logout`)
export const getListCourse = () => instanceAxios.get(`/course/list`)
export const postPayment = (info) => instanceAxios.post(`/receipt/create`, info)
export const getoneUser = (email) => instanceAxios.post(`/admin/user/get-by-email`, email)
export const postForgotPassword = (email) => instanceAxios.post(`/user/forgot-password`, email)
export const getMyCourse = () => instanceAxios.post("/course/owner")
export const getTransactionHistory = () => instanceAxios.post("/user/receipts")
export const postListBlog = () => instanceAxios.post("/blog/list")
export const postListBlogDetail = (idBlog) => instanceAxios.post("/blog/detail", idBlog)
