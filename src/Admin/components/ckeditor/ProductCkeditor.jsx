import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
// import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
// import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
// import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
// import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
// import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough";
// // import Highlight from "@ckeditor/ckeditor5-highlight/src/highlight";
// import Link from "@ckeditor/ckeditor5-link/src/link";
// import Image from "@ckeditor/ckeditor5-image/src/image";
// import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
// import Table from "@ckeditor/ckeditor5-table/src/table";
// import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote";
// import CodeBlock from "@ckeditor/ckeditor5-code-block/src/codeblock";
// import HorizontalLine from "@ckeditor/ckeditor5-horizontal-line/src/horizontalline";

function App(props) {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [data, setData] = useState("");
  const { setEditor, editor } = props;

  useEffect(() => {
    if (editor && !data) {
      setData(editor);
    }
    if (setEditor && data) {
      setEditor(JSON.stringify(data));
    }
  }, [editor, data, setEditor]);

  const handleEditorChange = (event, editor) => {
    const newData = editor.getData();
    setData(newData);
  };

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  function uploadFile(loader) {
    const formData = new FormData();
    const url = 'https://demo.tiksell.vn/upload/image';
  
    return new Promise((resolve, reject) => {
      loader.file.then((file) => {
        formData.append('thumbnail', file);
  
        fetch(url, {
          method: 'POST',
          body: formData,
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              reject('Error uploading file');
            }
          })  
          .then((success) => {
            resolve({ default: success?.full_link });
          })
          .catch((error) => {
            console.log(error); 
            reject(error);
          });
      });
    });
  }
  
  function CustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
      return {
        upload: () => {
          return uploadFile(loader);
        },
      };
    };
  }
  
  const editorStyle = {
    'min-height': '300px'
  };

  return (
    <div className="App">
      <h1 className="ml-6 mt-4 mb-4 text-[20px] font-medium abcd">Nội Dung</h1>

      {editorLoaded ? (
        <CKEditor
          editor={ClassicEditor}
          config={{
            extraPlugins: [
              CustomUploadAdapterPlugin,
              // Essentials,
              // Bold,
              // Italic,
              // Underline,
              // Strikethrough,
              // // Highlight,
              // Link,
              // Image,
              // ImageResize,
              // Table,
              // BlockQuote,
              // CodeBlock,
              // HorizontalLine,
            ],
          }}
          data={data}
          onChange={handleEditorChange}
          onReady={(editor) => {
            
            editor.editing.view.change((writer) => {
              writer.setStyle(editorStyle, editor.editing.view.document.getRoot());
            });
          }}
        />
      ) : (
        <div>Editor loading</div>
      )}
    </div>
  );
}

export default React.memo(App);
