import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

function CategoryCkeditor(props) {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [data, setData] = useState("");
  const { description, setDescription } = props;

  useEffect(() => {
    if (description && !data) {
      setData(description);
    }
    if(setDescription && data){
        setDescription(JSON.stringify(data));
        
    }
  }, [description, data]);

  const handleEditorChange = (event, editor) => {
    const newData = editor.getData();
    setData(newData);
  };
  

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  return (
    <div className="App">
      <h1 className="ml-6 mt-4 mb-4 text-[20px] font-medium abcd">Nội Dung</h1>

      {editorLoaded ? (
        <CKEditor
          editor={ClassicEditor}
          config={{
            ckfinder: {
              uploadUrl: ""
            }
          }}
          data={data}
          onChange={handleEditorChange}
          onReady={(editor) => {
            editor.editing.view.change((writer) => {
              writer.setStyle(
                { height: "150px" },
                editor.editing.view.document.getRoot()
              );
            });
          }}
        />
      ) : (
        <div>Editor loading</div>
      )}
    </div>
  );
}

export default React.memo(CategoryCkeditor);
