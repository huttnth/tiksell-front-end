import React, { useEffect, useState } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

function App(props) {
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [data, setData] = useState("");
  const { ckedit, setdataProduct ,editor,setEditor,hanldeCkeditor,datacurrent} = props;

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  useEffect(()=>{
 
    if(setdataProduct && ckedit){
      setdataProduct({ ...ckedit, ckedit: JSON.stringify(data) });
    }
  
   
  },[data])


  useEffect(() => {
    if(datacurrent && datacurrent !== ""){
    }
  }, [datacurrent])

  const handleEditorChange = (event, editor) => {
    const data = editor.getData();
    setData(data);
  };

  return (
    <div className="App">
      <h1 className="ml-6 mt-4 mb-4 text-[20px] font-medium abcd">Nội Dung</h1>

      {editorLoaded ? (
       <CKEditor
       editor={ClassicEditor}
       config={{
         ckfinder: {
           uploadUrl: ""
         }
       }}
       data={data}
       onChange={handleEditorChange}
       onReady={(editor) => {
         editor.editing.view.change(writer => {
           writer.setStyle(
             { height: '150px' },
             editor.editing.view.document.getRoot()
           );
         });
       }}
     />
     
      ) : (
        <div>Editor loading</div>
      )}

    </div>
  );
}

export default App;
