import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import React, { useState, useEffect } from "react";
import { Button, TextField, Card, CardContent, Grid, Box } from "@mui/material";
import {
  adminCategory_product,
  admin_createBlog,
  getoneBlog,
  updateBlog,
} from "../../../helpers/adminapi";
import { message } from "antd";

import Ckedit from "../../components/ckeditor/ProductCkeditor";
import { useParams } from "react-router-dom";
export default function AddBlogPage() {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState(null);


  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };

  const fetchData = async (id) => {
    try {
      const response = await getoneBlog({ id: id });
      setTitle(response.data?.data?.title);
      setDescription(response.data?.data?.description);
      setEditor(response.data?.data?.content);
      setdatapost({ id: response.data?.data?.category_id });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (id) {
      fetchData(id);
    }
  }, [id]);
  const [datacate, setDatacate] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await adminCategory_product();
        if (Array.isArray(response.data?.data)) {
          const ketqua = response.data.data.filter(value => value.status !== 0);
          setDatacate(ketqua);
        }
        } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
  };
  const [editor, setEditor] = useState("");
  const createBlog = async (e) => {
    if (!title || !description || !image || !editor || !datapost.id) {
      message.error("Vui lòng điền đầy đủ thông tin.", 2);
      return;
    }
    if (title.length > 100 || description.length > 160  || editor.length > 1560 ) {
      message.error("Quá Dài.", 2);
      return;
    }
    try {
      const formData = new FormData();
      formData.append("thumbnail", image);
      formData.append("title", title);
      formData.append("description", description);
      formData.append("blog_content", editor);
      formData.append("category_id", datapost.id);

      let create_blogs;

      if (id) {
        formData.append("id", id);
        create_blogs = await updateBlog(formData);
      } else {
        create_blogs = await admin_createBlog(formData);
      }
      if (create_blogs.status === 200) {
        if (id) {
          message.success("Bài Viết Đã Được Cập Nhật", 2);
        } else {
          message.success("Bài Viết Đã Được Thêm", 2);
        }
        setTimeout(() => {
          window.location.href = "/add-blog";
        }, 1000);
      } else {
        message.error(create_blogs.data.message);
      }
    } catch (error) {
      message.error(error.response.data.message);
    }
  };

  const [datapost, setdatapost] = useState();
  const handleChooseCategory = (value) => {
    setdatapost(value);
  };
  return (
    <Container>
      <div className="flex items-start justify-between">
        <Typography variant="h4" sx={{ mb: 1 }}>
          {id ? "Sửa Blog" : "Thêm Blog"}
        </Typography>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          onClick={() => createBlog()}
        >
          {id ? "Cập nhật Blog" : "Tạo Blog"}
        </Button>
      </div>
      <Grid container spacing={2} sx={{ mt: 1 }}>
        <Grid item xs={12} md={6} lg={8}>
          <Card>
            <CardContent>
              <form onSubmit={handleSubmit}>
                <TextField
                  label="Tiêu đề"
                  variant="outlined"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Mô tả"
                  variant="outlined"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  fullWidth
                  multiline
                  rows={4}
                  margin="normal"
                />
                <div>
                  <Ckedit
                    editor={editor}
                    setEditor={setEditor}
                  ></Ckedit>
                </div>
              </form>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Card>
            <CardContent>
              <div className="p-6 bg-white max-h[350px] overflow-auto">
                <div className="flex items-center cursor-pointer justify-between bg-white p-4">
                  <h1 className="text-xl text-[#615dff] font-medium ">
                    Danh Mục
                  </h1>
                  <div className="w-8 h-8 rounded-full bg-[#615dff] flex items-center duration-500 justify-center text-white"></div>
                </div>
                {datacate.length <= 0 ? null : (
                  <div className="mt-6">
                    {datacate.map((value, index) => {
                      return (
                        <div
                          onClick={() => {
                            handleChooseCategory(value);
                          }}
                          key={index}
                          className="my-2 ml-1  flex items-center "
                        >
                          <div
                            className={`w-5 h-5 mr-3 bg-white border cursor-pointer hover:border-[#615dff] duration-300 rounded-full   ${
                              datapost?.id === value.id
                                ? "!bg-[#615dff] border-[#8684fc]"
                                : ""
                            }`}
                          ></div>
                          <h3
                            className="text-live"
                            style={{
                              display: "block",
                              display: "-webkit-box",
                              maxWidth: "200px",
                              /* height: "43px", */
                              /* margin: "0 auto", */
                              fontSize: "14px",
                              lineHeight: "1.2",
                              WebkitLineClamp: 1,
                              WebkitBoxOrient: "vertical",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                            }}
                          >
                            {value.name}
                          </h3>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
              <div style={{ borderTop: "2px solid" }}>
                <div className="w-full  rounded-xl overflow-hidden duration-500	mb-6">
                  <div className="flex items-center cursor-pointer justify-between bg-white p-4">
                    <h1 className="text-xl text-[#615dff] font-medium ">
                      Ảnh đại diện
                    </h1>
                    <div className="w-8 h-8 rounded-full bg-[#615dff] flex items-center duration-500 justify-center text-white"></div>
                  </div>
                  <div></div>
                </div>
                <div>
                  {image && (
                    <div className="w-[100px] h-[100px]">
                      <img
                        src={URL.createObjectURL(image)}
                        alt="Selected_Image"
                        className="!w-full !max-h-[500px]"
                      />
                    </div>
                  )}
                 
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    className="mb-4"
                  />
                </div>
              </div>
            </CardContent>
          </Card>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              marginTop: "30px",
            }}
          ></Box>
        </Grid>
      </Grid>
    </Container>
  );
}
