import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import React, { useState } from "react";
import Editv2 from "../../components/ckeditor/app";
import { Button, TextField, Card, CardContent, Grid, Box } from "@mui/material";
import { useParams } from "react-router-dom";

export default function EditBlogPage() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState(null);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Title:", title);
    console.log("Description:", description);
    console.log("Image:", image);
  };
  const dataCategory = [
    { id: 1, name: "TiTok Shop" },
    { id: 2, name: "TikSell" },
    { id: 3, name: "Nodejs" },
    { id: 4, name: "Reactjs" },
  ];

  const [datapost, setdatapost] = useState();
  const handleChooseCategory = (value) => {
    setdatapost(value);
  };
  const { id } = useParams();
  return (
    <Container>
   <div className="flex  items-start justify-between">
   <Typography variant="h4" sx={{ mb: 1 }}>
        Edit-Blog/{id}
      </Typography>
      <Button type="submit" variant="contained" color="primary">
       Save
      </Button>
   </div>
      <Grid container spacing={2} sx={{mt:1}}>
        <Grid item xs={12} md={6} lg={8}>
          <Card>
            <CardContent>
              <form onSubmit={handleSubmit}>
                <TextField
                  label="Title"
                  variant="outlined"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Description"
                  variant="outlined"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  fullWidth
                  multiline
                  rows={4}
                  margin="normal"
                />

                <Editv2></Editv2>
              </form>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Card>
            <CardContent>
              <div className="p-6 bg-white">
              <div className="flex items-center cursor-pointer justify-between bg-white p-4">
                    <h1 className="text-xl text-[#615dff] font-medium ">Category</h1>
                    <div className="w-8 h-8 rounded-full bg-[#615dff] flex items-center duration-500 justify-center text-white"></div>
                  </div>
                {dataCategory.length <= 0 ? null : (
                  <div className="mt-6">
                    {dataCategory.map((value, index) => {
                      return (
                        <div
                          onClick={() => {
                            handleChooseCategory(value);
                          }}
                          key={index}
                          className="my-2 ml-1  flex items-center "
                        >
                          <div
                            className={`w-5 h-5 mr-3 bg-white border cursor-pointer hover:border-[#615dff] duration-300 rounded-full   ${
                              datapost?.id === value.id
                                ? "!bg-[#615dff] border-[#8684fc]"
                                : ""
                            }`}
                          ></div>
                          <h3 className="text-live">{value.name}</h3>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
              <div style={{borderTop:'2px solid'}}>
              
                <div className="w-full  rounded-xl overflow-hidden duration-500	mb-8">
                  <div className="flex items-center cursor-pointer justify-between bg-white p-4">
                    <h1 className="text-xl text-[#615dff] font-medium ">Ảnh đại diện</h1>
                    <div className="w-8 h-8 rounded-full bg-[#615dff] flex items-center duration-500 justify-center text-white"></div>
                  </div>
                  <div></div>
                </div>
                <div>
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    className="mb-4"
                  />
                  {image && (
                    <div className="w-full">
                      <img
                        src={URL.createObjectURL(image)}
                        alt="Selected_Image"
                        className="!w-full !max-h-[500px]"
                      />
                    </div>
                  )}
                </div>
              </div>
            </CardContent>
          </Card>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              marginTop: "30px",
            }}
          ></Box>
        </Grid>
      </Grid>
    </Container>
  );
}
