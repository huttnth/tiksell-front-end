import { useEffect, useState } from 'react';
import { Table, Popover, Modal, Form, Input, Switch, message } from 'antd';
import Stack from '@mui/material/Stack';
// import Table from '@mui/material/Table';
import Button from '@mui/material/Button';

import Typography from '@mui/material/Typography';

import Iconify from '../../../components/iconify';

import { GetAllUser, deleteUser, updateUser } from '../../../../helpers/adminapi';
import { MoreOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
// ----------------------------------------------------------------------
import "./../userAdmin.css"
import { postRegister } from '../../../../helpers/apis';
import dayjs from "dayjs";

export default function UserPage() {

  const [idPopover, setIdPopover] = useState();
  const [users, setUsers] = useState([])
  const [openModal, setOpenModal] = useState(false);
  const [statusModal, setStatusModal] = useState()
  const [formData, setFormData] = useState({
  });
  const [modalDelete, setModalDelete] = useState(false)
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const handleOpenChange = (newOpen) => {
    setIdPopover(idPopover === newOpen ? null : newOpen);
  };
  const getUserAll = () => {
    GetAllUser()
      .then((res) => {

        if (res.data?.status === 200) {
          setUsers(res.data?.data.map(item => ({ ...item, key: item.id })))
        }
      })
      .catch((e) => {
        console.log("e", e)
      })
  }
  useEffect(() => {
    getUserAll()
  }, [])

  const handleDeleteUser = () => {
    deleteUser(selectedRowKeys)
      .then((res) => {

        if (res?.data?.status === 200) {
          message.success(res?.data?.message)
          getUserAll()
          setModalDelete(false)
        }
        if (res?.data?.status >= 400) {
          message.error(res?.data?.message)
        }
      })
      .catch((e) => {
        console.log("e", e)
      })

  }
  const columns = [

    {
      title: 'Tên người dùng',
      key: "name",
      dataIndex: 'name',
    },
    {
      title: 'Số điện thoại',
      key: "phone_number",
      dataIndex: 'phone_number',
    },
    {
      title: 'Email',
      key: "email",
      dataIndex: 'email',
    },
    {
      title: 'Thời gian tạo',
      key: "created_at",
      dataIndex: 'created_at',
      render: (value, record) => <p>{dayjs(value).format("YYYY-MM-DD")}</p>,

    },
    {
      title: 'Thiết bị truy cập',
      key: "",
      dataIndex: '',
    },
    {
      title: 'Trạng thái tài khoản',
      dataIndex: 'status',
      key: "status",
      render: ((value, row) => (
        <div className='h-f'>{value === 1 ? "Hoạt động" : "Ngừng hoạt động"}</div>
      ))
    },
    
    {
      title: 'Hành động ',
      className: "flex justify-center",
      dataIndex: 'id',
      key: 'id',
      render: (id, row) => (
        <div className='flex justify-center'>
          <Popover
            content={<div>
              <button onClick={() => {
                setStatusModal(1)
                setFormData(row)
                setOpenModal(true)
                setIdPopover(null)
              }} className='flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]'> <EditOutlined className='' style={{ color: "red" }} />  <span style={{ color: "red", fontSize: "20px" }} className='text-red'>Sửa</span></button>
              <button onClick={() => {
                setIdPopover(null)
                setSelectedRowKeys([id])
                setModalDelete(true)
              }} className='flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]'> <DeleteOutlined /> <span style={{ fontSize: "20px" }}>xóa</span></button>
            </div>}
            trigger="click"
            open={idPopover === id ? true : false}
            onOpenChange={()=>setIdPopover(false)}


          >
            <MoreOutlined onClick={() => handleOpenChange(id)} />
          </Popover>

        </div>
      )
    },
  ];
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  const handleCreateUser = async () => {
    postRegister(formData)
      .then((res) => {
        if (res?.data?.status === 200) {
          message.success(res?.data?.message)
          setTimeout(() => {
            setOpenModal(false)
            getUserAll()
          }, 1000)
        }
        if (res?.data?.status >= 400) {
          message.error(res?.data?.message)
        }
      })
      .catch((e) => {
        message.error(e?.responsive?.data.message)
      })
      .finally(() => {

      })
  }
  const handleUpdateUser = async () => {
    updateUser(formData)
      .then((res) => {
        if (res?.data?.status === 200) {
          message.success(res?.data?.message)
          setTimeout(() => {
            setOpenModal(false)
            getUserAll()
          }, 1000)
        }
        if (res?.data?.status >= 400) {
          message.error(res?.data?.message)
        }

      })
      .catch((e) => {
        message.error(e?.responsive?.data.message)
      })
  }
  const onSelectChange = (newSelectedRowKeys) => {

    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  }
  return (
    <div className='  shadow-lg w-[90%] rounded-md m-auto bg-[#fff] p-10'>
      <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
        <Typography variant="h4">Users</Typography>

        <Button onClick={() => {
          setStatusModal(0)
          setOpenModal(true)
        }} variant="contained" color="inherit" startIcon={<Iconify icon="eva:plus-fill" />}>
          Tài khoản người dùng
        </Button>
      </Stack>
      <Table bordered columns={columns} rowSelection={rowSelection} dataSource={users} />
      <Modal
        title={<div className='flex justify-center text-[20x] '>{statusModal === 0 ? "Tạo tài khoản người dùng" : "Sửa thông tin tài khoản"}</div>}
        centered
        open={openModal}
        onOk={() => setOpenModal(false)}
        onCancel={() => {
          setOpenModal(false)
          setFormData({})
        }}
        footer={false}
      >
        <Form

          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}

          autoComplete="off"
          className='user_admin mt-5 text-center flex flex-col gap-4'
        >

          <div className='flex gap-2 justify-between'>
            <label >Họ và tên :</label> <Input name='name' className='w-[330px]' value={formData.name} onChange={handleInputChange} />
          </div>
          <div className='flex gap-2 justify-between'>
            <label >Mật khẩu :</label><Input name='password' className='w-[330px]' value={formData.password} onChange={handleInputChange} />
          </div>
          <div className='flex gap-2 justify-between'>
            <label >Email :</label>
            <Input disabled={statusModal ? true : false} className='w-[330px]' value={formData.email} name='email' onChange={handleInputChange} />
          </div>
          <div className='flex gap-2 justify-between'>
            <label >Số điện thoại :</label>
            <Input name='phone_number' className='w-[330px]' value={formData.phone_number} onChange={handleInputChange} />
          </div>
          {
            statusModal
            &&
            <div className='flex gap-3 '>
              <label >Trạng thái tài khoản </label>
              <Switch className={formData.status === 0 && `color`} checked={formData.status} onChange={(e) => {
                setFormData({
                  ...formData,
                  status: e === true ? 1 : 0,
                });
              }} />
            </div>
          }
          {
            statusModal
              ?
              <button onClick={handleUpdateUser} className=' bg-[blue] text-[#fff] px-3 py-2 rounded-md m-auto ' type="primary" html="submit">
                Xác nhận
              </button>
              :
              <button onClick={handleCreateUser} className=' bg-[blue] text-[#fff] px-3 py-2 rounded-md m-auto ' type="primary" html="submit">
                Tạo mới
              </button>

          }

        </Form>
      </Modal>
      <div className='modal_delete'>
        <Modal
          title={<div className='flex justify-center text-[20x] '> </div>}
          centered
          open={modalDelete}
          onOk={() => setModalDelete(false)}
          onCancel={() => {
            setModalDelete(false)

          }}
          className='modal_delete'
          footer={false}
          width={300}
          closable={false}
        >
          <div className='flex justify-center gap-3'>
            <button className='min-w-[100px] rounded-md py-2 bg-[blue] text-[#fff] ' onClick={handleDeleteUser}>Xác nhận</button>
            <button className='min-w-[100px] rounded-md py-2 bg-[red] text-[#fff] ' onClick={() => setModalDelete(false)}>Hủy</button>
          </div>
        </Modal>
      </div>

    </div >
  );
}
