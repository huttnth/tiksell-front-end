import React, { useState } from "react";
import Dropdown from "./Dropdown";
import Typography from "@mui/material/Typography";
import Ckedit from "../../components/ckeditor/app";
import SaveIcon from "@mui/icons-material/Save";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Button,
  Grid,
  TextField
} from "@mui/material";
import { useEffect } from "react";
import { adminCategory_product,prdocuctAdmin} from "../../../helpers/adminapi";
 const AddProductPage = (props) => {
  const [selectedValue, setSelectedValue] = useState("");

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };
  const [imgg, setImgg] = useState(null);
  const [datacate, setDatacate] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await adminCategory_product();
        setDatacate(response.data?.data);
        console.log(response, "response");
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);
  const data = [];

  const [datas, setDatas] = useState(data);

  const [dataProduct, setdataProduct] = useState({
    title: "",
    des: "",
    gia: "",
    gianiemyet: "",
    category: "",
    ckedit: "",
    videothumbnail: null,
    immProduct: null,
  });
  const [valueToggle, setValueToggle] = useState({
    idchaToggle: undefined,
    idconToggle: undefined,
  });


  const handleToggleEdit = (idcha, idcon) => {
    if (
      valueToggle.idchaToggle === idcha &&
      valueToggle.idconToggle === idcon
    ) {
      setValueToggle({ idchaToggle: undefined, idconToggle: undefined });
    } else {
      setValueToggle({ idchaToggle: idcha, idconToggle: idcon });
    }
  };

  const handleCreate = (option, idcha) => {
    if (option === "cha") {
      let newdata = [
        ...datas,
        {
          title: "",
          id: datas.length + 1,
          item: [],
        },
      ];
      setValueToggle({ idchaToggle: datas.length + 1, idconToggle: undefined });
      setDatas(newdata);
    } else {
      let cloneData = [...datas];

      let finddataIndex = cloneData.findIndex((values) => values.id === idcha);
      cloneData[finddataIndex].item = [
        ...cloneData[finddataIndex].item,
        {
          name: ``,
          video: "",
          id: cloneData[finddataIndex].item.length + 1,
        },
      ];
      setValueToggle({
        idchaToggle: idcha,
        idconToggle: cloneData[finddataIndex].item.length,
      });
      setDatas(cloneData);
    }
  };

  const handleCategoryChange = (event) => {
    const newValue = event.target.value;
    setdataProduct((prevData) => ({
      ...prevData,
      category: newValue,
    }));
  };

  // console.log({...dataProduct,module:datas},'1111111111')
console.log(dataProduct.videothumbnail,'111111111')
  const create_product = async () => {
    try {
      const formData = new FormData();
      formData.append('title', dataProduct.title);
      formData.append('des', dataProduct.des);
      formData.append('gia', dataProduct.gia);
      formData.append('gianiemyet', dataProduct.gianiemyet);
      formData.append('videothumbnail', dataProduct.videothumbnail);
      formData.append('immProduct', dataProduct.immProduct);
      formData.append('category', dataProduct.category);
      formData.append('ckedit', dataProduct.ckedit);
      const response = await prdocuctAdmin(formData);
      console.log(response.data, 'data');
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="w-full ">
      <div className="flex justify-between items-center">
        <Typography variant="h4" sx={{ mb: 1 }}>
          Thêm Sản Phẩm
        </Typography>
      </div>
      <Grid container spacing={2}>
        <Grid item xs={12} lg={7}>
          <TextField
            label="Tiêu đề"
            variant="outlined"
            fullWidth
            margin="normal"
            value={dataProduct.title}
            onChange={(e) => {
              const newValue = e.target.value;
              setdataProduct((prevData) => ({ ...prevData, title: newValue }));
            }}
          />
          <TextField
            label="Mô tả sản phẩm"
            variant="outlined"
            fullWidth
            margin="normal"
            multiline
            rows={4}
            value={dataProduct.des}
            onChange={(e) => {
              const newValue = e.target.value;
              setdataProduct((prevData) => ({ ...prevData, des: newValue }));
            }}
          />
          <Grid container spacing={2}>
            <Grid item xs={6} lg={6}>
              <TextField
                label="Giá niêm yết"
                variant="outlined"
                fullWidth
                margin="normal"
                type="number"
                value={dataProduct.gianiemyet}
                onChange={(e) => {
                  const newValue = e.target.value;
                  setdataProduct((prevData) => ({
                    ...prevData,
                    gianiemyet: newValue,
                  }));
                }}
              />
            </Grid>
            <Grid item xs={6} lg={6}>
              <TextField
                label="Giá sản phẩm"
                variant="outlined"
                fullWidth
                margin="normal"
                type="number"
                value={dataProduct.gia}
                onChange={(e) => {
                  const newValue = e.target.value;
                  setdataProduct((prevData) => ({
                    ...prevData,
                    gia: newValue,
                  }));
                }}
              />
            </Grid>
            <div className="ml-6 w-full">
              <Ckedit
                ckedit={dataProduct}
                setdataProduct={setdataProduct}
              ></Ckedit>
            </div>
            <div className="flex justify-start flex-col mt-5 mb-2 ml-6">
              <div className="flex justify-center items-center">
                <p className="text-[20px] font-medium mr-2">Ảnh đại diện :</p>
                <input
                  type="file"
                  onChange={(e) => {
                    const reader = new FileReader();
                    const file = e.target.files[0];

                    reader.onload = (e) => {
                      const newImg = e.target.result;
                      setImgg(newImg);
                      setdataProduct((prevData) => ({
                        ...prevData,
                        immProduct: file,
                      }));
                    };

                    if (file) {
                      reader.readAsDataURL(file);
                    }
                  }}
                />
              </div>

              {imgg ? (
                <img
                  src={imgg}
                  alt=""
                  className="w-[100px] h-[100px] float-left"
                />
              ) : null}
            </div>

            <div className="flex justify-center items-center mt-5 mb-2 ml-6">
              <p className="text-[20px] font-medium mr-2">Video Giới Thiệu</p>
              <input
                type="file"
                accept="video/*"
                onChange={(e) => {
                  const reader = new FileReader();
                  const file = e.target.files[0];

                  reader.onload = (e) => {
                    const newImg = e.target.result;
                    setImgg(newImg);
                    setdataProduct((prevData) => ({
                      ...prevData,
                      videothumbnail: file,
                    }));
                  };

                  if (file) {
                    reader.readAsDataURL(file);
                  }
                }}
              />
            </div>
          </Grid>
        </Grid>
        <Grid className="mt-6 ml-20" xs={12} lg={4}>
          <FormControl component="fieldset">
            <FormLabel
              component="legend"
              className="text-[35px] font-medium mb-5"
            >
              Danh Mục Sản Phẩm
            </FormLabel>
            <RadioGroup value={selectedValue} onChange={handleChange}>
              {datacate
                ? datacate.map((value, index) => (
                    <FormControlLabel
                      key={index}
                      value={value.id}
                      control={<Radio />}
                      label={value.name}
                      // checked={dataProduct.category === value.id}
                      onChange={handleCategoryChange}
                    />
                  ))
                : null}
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>
      <Typography variant="h4" sx={{ mb: 1, mt: 2 }}>
        Thêm Nội dung khoá học
      </Typography>
      {datas.map((value, index) => {
        return (
          <Dropdown
            titleElement={
              <div className="flex w-full group/item items-center justify-between">
                {valueToggle.idchaToggle === value.id &&
                !valueToggle.idconToggle ? (
                  <input
                    onChange={(e) => {
                      datas[index].title = e.target.value;
                      let newdata = [...datas];
                      setDatas(newdata);
                    }}
                    placeholder="Tên Nội Dung"
                    value={value.title}
                    type="text"
                    className="border mr-8 px-4 py-2 shadow-md rounded-lg group/item hover:bg-slate-200 duration-300"
                  />
                ) : (
                  <div className="p-2">{`${value.title}`}</div>
                )}

                <div className="group-hover/item:!block hidden mr-4 animationv1">
                  <button
                    className={`mr-4 duration px-3 py-1.5 bg-yellow-400 text-black hover:!bg-yellow-500 rounded-lg`}
                    onClick={() => {
                      handleToggleEdit(value.id);
                    }}
                  >
                    {valueToggle.idchaToggle === value.id &&
                    !valueToggle.idconToggle ? (
                      <SaveIcon></SaveIcon>
                    ) : (
                      <EditIcon></EditIcon>
                    )}
                  </button>
                  <button
                    className="text-white bg-red-400 hover:bg-red-500 duration-300 text-sm px-3 py-2 rounded-md "
                    onClick={() => {
                      const filteredDatas = datas.filter(
                        (valuee, index) => value.id !== valuee.id
                      );
                      setDatas([...filteredDatas]);
                    }}
                  >
                    <DeleteIcon></DeleteIcon>
                  </button>
                </div>
              </div>
            }
          >
            <div>
              {value.item.map((item, index1) => {
                return (
                  <div className="group/item p-3 pl-12 border">
                    <div className="flex w-full items-center justify-between">
                      {valueToggle.idconToggle === item.id &&
                      valueToggle.idchaToggle === value.id ? (
                        <div className="flex">
                          <input
                            required
                            onChange={(e) => {
                              datas[index].item[index1].name = e.target.value;
                              let newdata = [...datas];
                              setDatas(newdata);
                            }}
                            value={item.name}
                            type="text"
                            className="border mr-8 px-4 py-2 shadow-md rounded-lg group/item hover:bg-slate-200 duration-300"
                          />
                          <div>
                            <div className="flex justify-start items-center">
                              <p className="mr-3 text-[20px] font-medium">
                                link video :
                              </p>
                              <input
                                required
                                onChange={(e) => {
                                  datas[index].item[index1].video =
                                    e.target.files[0];
                                  let newdata = [...datas];
                                  setDatas(newdata);
                                }}
                                // value={item.video}
                                type="file"
                                accept="video/*"
                                className="border mr-8 px-4 py-2 shadow-md rounded-lg group/item hover:bg-slate-200 duration-300"
                              />
                            </div>
                          </div>{" "}
                        </div>
                      ) : (
                        <div className="p-2">{`${item.name}`}</div>
                      )}

                      <div className="hidden group-hover/item:block animationv1">
                        <button
                          className={`mr-4 duration px-3 py-1.5 bg-yellow-400 hover:!bg-yellow-500 rounded-lg`}
                          onClick={() => {
                            handleToggleEdit(value.id, item.id);
                          }}
                        >
                          {valueToggle.idconToggle === item.id &&
                          valueToggle.idchaToggle === value.id ? (
                            <SaveIcon></SaveIcon>
                          ) : (
                            <EditIcon></EditIcon>
                          )}
                        </button>
                        <button
                          className="text-white bg-red-400 hover:bg-red-500 duration-300 text-sm px-3 py-2 rounded-md"
                          onClick={() => {
                            const deleteItem = [...datas];
                            deleteItem[index].item = deleteItem[
                              index
                            ].item.filter(
                              (valuee, index) => item.id !== valuee.id
                            );
                            setDatas([...deleteItem]);
                          }}
                        >
                          <DeleteIcon></DeleteIcon>
                        </button>
                      </div>
                    </div>
                  </div>
                );
              })}
              <div
                onClick={() => {
                  if (
                    datas[index].item.length > 0 &&
                    datas[index].item[datas[index].item.length - 1].name === ""
                  ) {
                    alert("Vui lòng nhập giá trị cho item");
                  } else {
                    handleCreate("con", value.id);
                  }
                }}
                className="bg-white text-center text-xl cursor-pointer	 text-black font-medium hover:shadow-xl  border rounded-lg shadow-lg hover:scale-105 duration-300"
              >
                + Add Video
              </div>
            </div>
          </Dropdown>
        );
      })}
      <div
        onClick={() => {
          if (datas.length > 0 && datas[datas.length - 1].title === "") {
            return alert("Lỗi");
          } else {
            handleCreate("cha");
          }
        }}
        className="bg-white text-center text-xl cursor-pointer	 text-black font-medium hover:shadow-xl w-[60px] border rounded-lg shadow-lg hover:scale-105 duration-300"
      >
        +
      </div>
      <div className="flex justify-center items-center mt-5">
        <Button type="submit" variant="contained" color="primary" onClick={()=>create_product()}>
          Thêm sản phẩm
        </Button>
      </div>
    </div>
  );
};


export default AddProductPage