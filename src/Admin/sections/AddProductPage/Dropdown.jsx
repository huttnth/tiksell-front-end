import React, { useState } from "react";
import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";


function Dropdown({ children, titleElement }) {
  const [active, setActive] = useState(false);
  const handleActive = () => {
    setActive(!active);
  };
  return (
    <div className="w-full  rounded-xl  overflow-hidden duration-500	!mb-[10px]">
      <div
       
        className="flex items-center border cursor-pointer justify-between bg-white p-3"
      >
        <h1 className="text-xl text-[#615dff] font-medium w-full ">{titleElement}</h1>
        <div  onClick={handleActive} className="w-10 h-10 rounded-full  bg-[#615dff] flex items-center duration-500 justify-center text-white">
          {active ? <IoIosArrowUp /> : <IoIosArrowDown />}
        </div>
      </div>
      <div
        className={`overflow-hidden transition-max-h ease-in-out duration-500 ${
          active ? "max-h-[5000px]" : "max-h-0"
        }`}
      >
        {children}
      </div>
    </div>
  );
}

export default Dropdown;
