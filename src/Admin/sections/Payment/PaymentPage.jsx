import { useEffect, useState } from 'react';
import { Table, Popover, message } from 'antd';
import Stack from '@mui/material/Stack';
import "./payment.css"
import Typography from '@mui/material/Typography';
import { postReceipt, postUpdateReceipt } from '../../../helpers/adminapi';
import { MoreOutlined } from '@ant-design/icons';
import { formatNumber } from '../../../helpers/common'
import dayjs from 'dayjs';

export default function PaymentPage() {
  const [loading, setLoading] = useState(false);
  const [course, setCourse] = useState([]);
  const [activePopoverId, setActivePopoverId] = useState(null);
  const [idPopover, setIdPopover] = useState();

  const getUserCourse = () => {
    setLoading(true);
    postReceipt()
      .then((res) => {
        if (res.data?.status === 200) {
          setCourse(res.data?.data.map(item => ({ ...item, key: item.id })));
        }
      })
      .catch((e) => {
        console.log("e", e);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  useEffect(() => {
    getUserCourse();
  }, []);

  const handleOpenChange = (id) => {
    setActivePopoverId(activePopoverId === id ? null : id);
  }

  
  const handleUpdateReceipt = (status, id) => {
    const input = {
      status,
      id
    };

    postUpdateReceipt(input)
      .then((res) => {
        if (res.data.status === 200) {
          message.success(res.data.message);
          getUserCourse();
          setActivePopoverId(null);
        }
        if (res.data.status >= 400) {
          message.error(res.data.message);
        }
      })
      .catch((e) => {
        console.log("e", e);
      }).finally(() => {
        setActivePopoverId(null); 
      });
  }

  const columns = [
      {
      title: 'ID',
      key: "id",
      dataIndex: 'id',

    },
    {
      title: 'Tên người dùng',
      key: "name",
      dataIndex: 'name',
      render: (user) => {
        return <div>{user || "Khách vãng lai"}</div>
      }
    },
    {
      title: 'Tên đăng nhập',
      key: "user",
      dataIndex: 'user',
      render: (user) => {
        return <div>{user.email}</div>
      }
    },
    {
      title: 'Số điện thoại',
      key: "phone_number",
      dataIndex: 'phone_number',
    },
    {
      title: 'Tên khóa học',
      key: "course",
      dataIndex: 'course',
      render: (value) => {
        return <div>{value.title}</div>
      }
    },
    {
      title: 'Giá khóa học',
      key: "amount",
      dataIndex: 'amount',
      render: (value) => {
        return <div>{formatNumber(value)}</div>
      }
    },
    {
      title: 'Ngày tạo',
      key: "created_at",
      dataIndex: 'created_at',
      render: (value, record) => <p>{dayjs(value).format("YYYY-MM-DD")}</p>,

    },
    {
      title: 'Trạng thái ',
      dataIndex: 'status',
      key: "status",
    },
    {
      title: 'Hành động',
      className: "flex justify-center",
      dataIndex: 'id',
      key: 'id',
      render: (id) => (
        <div className='flex justify-center'>
          <Popover
            trigger="click"
            content={<div className='flex flex-col p-4 gap-2'>
              <button onClick={() => handleUpdateReceipt(1, id)} className='py-2 min-w-[120px] text-[#fff] rounded-md bg-[#6edf4f]  '>Đã thanh toán</button>
              <button onClick={() => handleUpdateReceipt(3, id)} className='py-2 min-w-[120px] text-[#fff] rounded-md bg-[#c6d648]  '>Khánh vãng lai</button>
              <button onClick={() => handleUpdateReceipt(4, id)} className='py-2 min-w-[120px] text-[#fff] rounded-md bg-[#4f4fdf]  '>Chờ thanh toán</button>
              <button onClick={() => handleUpdateReceipt(0, id)} className='py-2 min-w-[120px] text-[#fff] rounded-md bg-[red] ' >Hủy</button>
            </div>}
            open={activePopoverId === id}
            onOpenChange={()=>setActivePopoverId(null)}
          >
            <MoreOutlined onClick={() => handleOpenChange(id)} />
          </Popover>
        </div>
      )
    },
  ];

  return (
    <div className='  shadow-lg w-[90%] rounded-md m-auto bg-[#fff] p-10'>
      <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
        <Typography variant="h4">Danh sách khóa học</Typography>
      </Stack>
      <Table loading={loading} bordered columns={columns} dataSource={course} />
    </div >
  );
}
