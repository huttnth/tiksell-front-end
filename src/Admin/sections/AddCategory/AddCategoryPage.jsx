import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import React, { useState } from "react";
import {
  Button,
  TextField,
  Card,
  CardContent,
  Grid,
  Box,
} from "@mui/material";
import Ckeditor from '../../components/ckeditor/app'
export default function AddCategoryPage() {
  const [key, setKey] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState
  const [image, setImage] = useState(null);
  

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log("Creator:", key);
    // console.log("Title:", title);
    // console.log("Description:", description);
    // console.log("Image:", image);
  };

  return (
    <Container>
      <Typography variant="h4" sx={{ mb: 5 }}>
        Add Category
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Card>
            <CardContent>
              <form onSubmit={handleSubmit}>
                <TextField
                  label="Title"
                  variant="outlined"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  fullWidth
                  margin="normal"
                />
                 <TextField
                  label="Key Word"
                  variant="outlined"
                  value={key}
                  onChange={(e) => setKey(e.target.value)}
                  fullWidth
                  margin="normal"
                />
                <Ckeditor></Ckeditor>
                
                <Box sx={{ display: "flex", justifyContent: "center", marginTop: '30px' }}>
                  <Button type="submit" variant="contained" color="primary">
                    Create Category
                  </Button>
                </Box>
              </form>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
