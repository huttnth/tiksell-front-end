
import Container from "@mui/material/Container";
import Grid from "@mui/material/Unstable_Grid2";
import Typography from "@mui/material/Typography";

import AppNewsUpdate from "../app-news-update";
import AppOrderTimeline from "../app-order-timeline";
import AppCurrentVisits from "../app-current-visits";
import AppWebsiteVisits from "../app-website-visits";
import AppWidgetSummary from "../app-widget-summary";
import { apiAdmin } from "../../../../helpers/adminapi";
import { useEffect, useState } from "react";
// ----------------------------------------------------------------------

export default function AppView() {
  const [data, setData] = useState([]);
  const [blogs, setBlogs] = useState([]);
  const [danhso, setDanhso] = useState("0");
  const [donhang, setDonhang] = useState("0");
  const [hocvien, setHocvien] = useState("0");
  const [khoahoc, setKhoahoc] = useState("0");
  const [chartdanhthu, setChartdanhthu] = useState([]);
  const [chartpie, setChartpie] = useState([])
  const [payment, setPayment] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await apiAdmin();
        setDanhso(response.data?.card_total?.doanh_so);
        setDonhang(response.data?.card_total?.don_hang);
        setHocvien(response.data?.card_total?.hoc_vien);
        setKhoahoc(response.data?.card_total?.khoa_hoc);
        setChartdanhthu(response?.data?.chart_column);
        setChartpie(response.data?.chart_pie)
        setPayment(response.data?.top_receipt)
        setBlogs(response.data?.blogs)
        setChartdanhthu(response.data?.chart_column)
        console.log(response.data.chart_column.labels, '1111111111111')
        setData(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);
  return (
    <Container maxWidth="xl">
      <Typography variant="h4" sx={{ mb: 5 }}>
        Hi, Welcome back 👋
      </Typography>

      <Grid container spacing={3}>
        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Khoá Học"
            total={khoahoc}
            color="success"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_bag.png" />}
          />
        </Grid>

        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Học Viên"
            total={hocvien}
            color="info"
            icon={
              <img alt="icon" src="/assets/icons/glass/ic_glass_users.png" />
            }
          />
        </Grid>

        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Đơn Hàng"
            total={donhang}
            color="warning"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_buy.png" />}
          />
        </Grid>

        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Danh Số"
            total={danhso}
            color="error"
            icon={
              <img alt="icon" src="/assets/icons/glass/ic_glass_message.png" />
            }
          />
        </Grid>

        <Grid xs={12} md={6} lg={8}>
          {chartdanhthu && chartdanhthu.labels && chartdanhthu.series ? (
            <AppWebsiteVisits
              title="Danh Thu"
              subheader="Biểu Đồ Danh Thu Trong Tháng"
              chart={chartdanhthu}
            />
          ) : (
            <div>Loading...</div>
          )}
        </Grid>



        <Grid xs={12} md={6} lg={4}>
          {chartpie ? (
            <AppCurrentVisits
              title="Khu Vực"
              chart={{ ['chartpie']: chartpie }}
            />
          ) : (
            <div>loadding ...</div>
          )}
        </Grid>
        <Grid xs={12} md={6} lg={8}>
          <AppNewsUpdate
            title="Bài viết mới"
            list={blogs ? blogs : []}
          />
        </Grid>

        <Grid xs={12} md={6} lg={4}>
          <AppOrderTimeline
            title="Lịch sử order"
            list={payment}
          />
        </Grid>

        {/* <Grid xs={12} md={6} lg={4}>
          <AppTrafficBySite
            title="Traffic by Site"
            list={[
              {
                name: 'FaceBook',
                value: 323234,
                icon: <Iconify icon="eva:facebook-fill" color="#1877F2" width={32} />,
              },
              {
                name: 'Google',
                value: 341212,
                icon: <Iconify icon="eva:google-fill" color="#DF3E30" width={32} />,
              },
              {
                name: 'Linkedin',
                value: 411213,
                icon: <Iconify icon="eva:linkedin-fill" color="#006097" width={32} />,
              },
              {
                name: 'Twitter',
                value: 443232,
                icon: <Iconify icon="eva:twitter-fill" color="#1C9CEA" width={32} />,
              },
            ]}
          />
        </Grid>

        <Grid xs={12} md={6} lg={8}>
          <AppTasks
            title="Tasks"
            list={[
              { id: '1', name: 'Create FireStone Logo' },
              { id: '2', name: 'Add SCSS and JS files if required' },
              { id: '3', name: 'Stakeholder Meeting' },
              { id: '4', name: 'Scoping & Estimations' },
              { id: '5', name: 'Sprint Showcase' },
            ]}
          />
        </Grid> */}
      </Grid>
    </Container>
  );
}
