import React, { useState } from "react";
import { Table, Tag } from "antd";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import EditIcon from "@mui/icons-material/Edit";
import { MoreOutlined } from "@ant-design/icons";
import { Popover } from "antd";

import DeleteIcon from "@mui/icons-material/Delete";
import Iconify from "../../../components/iconify";

import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { admin_blogs } from "../../../../helpers/adminapi";
import { delete_Blog } from "../../../../helpers/adminapi";
import { useEffect } from "react";
import { message } from "antd";

import dayjs from "dayjs";

function BlogViews() {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await admin_blogs();
      setData(response.data?.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteBlog = async (id) => {
    try {
      const response = await delete_Blog({ id: id });

      if (response.status === 200) {
        fetchData();
        message.success("Xoá Thành Công");
      } else {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error("Lỗi Kết Nối");
    }
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tiêu Đề",
      dataIndex: "title",
      key: "title",
      width: 150,
    },

    {
      title: "Mô Tả",
      dataIndex: "description",
      key: "description",
      width: 250,
      render: (text) => (
        <p style={{ maxWidth: '250px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
          {text}
        </p>
      )
    },
    {
      title: "Nội Dung",
      dataIndex: "content",
      key: "content",

      render: (text) => (
        <div dangerouslySetInnerHTML={{ __html:text.replace(/\\/g, '').replace(/"/g, '') }} className="text-ellipsis line-clamp-2
        max-w-[400px]
        "></div>
      ),
    },
    {
      title: "Ngày Tạo",
      key: "created_at",
      dataIndex: "created_at",
      width: 150,
      render: (value, record) => <p>{dayjs(value).format("YYYY-MM-DD")}</p>,
    },

    {
      title: "Action",
      key: "action",
      align:'center',
      render: (id, row) => (
        <div className="flex justify-center">
          <Popover
            content={
              <div>
                <button
                  onClick={(id) => {
                    setCheck(true);
                    nav(`/edit-blog/${row.id}`);
                  }}
                  className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
                >
                  <EditIcon style={{ color: "red" }} />
                  <span
                    style={{ color: "red", fontSize: "20px" }}
                    className="text-red"
                  >
                    Sửa
                  </span>
                </button>
                <button
                  onClick={() => handleDeleteBlog(row.id)}
                  className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
                >
                  <DeleteIcon />
                  <span style={{ fontSize: "20px" }}>Xóa</span>
                </button>
              </div>
            }
            trigger="click"
          >
            <MoreOutlined />
          </Popover>
        </div>
      ),
    },
  ];

  const nav = useNavigate();
  const [check, setCheck] = useState(false);
  const check_icon = () => {
    setCheck(true);
  };
  return (
    <div>
      <Paper>
        <Paper></Paper>

        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            pl: { sm: 2 },
            pr: { xs: 1, sm: 1 },
          }}
        >
          <Typography variant="h4" id="tableTitle" component="div">
            Blogs
          </Typography>
          <Button
            variant="contained"
            onClick={() => nav("/add-blog")}
            color="inherit"
            startIcon={<Iconify icon="eva:plus-fill" />}
          >
            Add Blog
          </Button>
        </Toolbar>
        <Table columns={columns} dataSource={data} />
      </Paper>
    </div>
  );
}

export default BlogViews;
