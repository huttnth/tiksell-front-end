import React, { useState } from "react";
import { Table, Tag } from "antd";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import EditIcon from "@mui/icons-material/Edit";
import { MoreOutlined } from "@ant-design/icons";
import { Popover } from "antd";

import DeleteIcon from "@mui/icons-material/Delete";
import Iconify from "../../../components/iconify";

import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { adminProduct } from "../../../../helpers/adminapi";
import { useEffect } from "react";

function ProductsView() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await adminProduct();
        setData(response.data?.data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);
  const columns = [
    {
      title: "Tên Khoá Học",
      dataIndex: "title",
      key: "title",
      render: (text) => (
        <p style={{ maxWidth: '200px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
          {text}
        </p>
      )
    },
    {
      title: "Danh Mục",
      key: "type",
      width: 150,
      render: (type, id) => <div  style={{ maxWidth: '200px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }} >{type.type.description}</div>,
    },

    {
      title: "Mô tả",
      dataIndex: "description",
      key: "description",
      render: (text) => (
        <p style={{ maxWidth: '250px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
          {text}
        </p>
      )
    },
    {
      title: "Số Người Học",
      dataIndex: "rate_people",
      key: "rate_people",
      width: 150,
    },
    {
      title: "Giá",
      key: "price",
      dataIndex: "price",
      width: 150,
    },
    {
      title: "Giá Niêm Yết",
      key: "unit_price",
      dataIndex: "unit_price",
      width: 150,
    },
    {
      title: "Hành Động",
      key: "action",
      render: (id, row) => (
        <div className="flex justify-center">
          <Popover
            content={
              <div>
                <button
                  onClick={() => {
                    setCheck(true);
                    nav(`/edit-products/${row.id}`);
                  }}
                  className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
                >
                  <EditIcon style={{ color: "red" }} />
                  <span
                    style={{ color: "red", fontSize: "20px" }}
                    className="text-red"
                  >
                    Sửa
                  </span>
                </button>
                <button
                  onClick={() => {
                    // Handle delete action here
                  }}
                  className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
                >
                  <DeleteIcon />
                  <span style={{ fontSize: "20px" }}>Xóa</span>
                </button>
              </div>
            }
            trigger="click"
          >
            <MoreOutlined />
          </Popover>
        </div>
      ),
    },
  ];

  const nav = useNavigate();
  const [check, setCheck] = useState(false);

  const check_icon = () => {
    setCheck(true);
  };
  return (
    <div>
      <Paper>
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            pl: { sm: 2 },
            pr: { xs: 1, sm: 1 },
          }}
        >
          <Typography variant="h4" id="tableTitle" component="div">
            Product
          </Typography>
          <Button
            variant="contained"
            onClick={() => nav("/add-products")}
            color="inherit"
            startIcon={<Iconify icon="eva:plus-fill" />}
          >
            Add Product
          </Button>
        </Toolbar>
        <Table columns={columns} dataSource={data} />
      </Paper>
    </div>
  );
}

export default ProductsView;
