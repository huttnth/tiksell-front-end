import SvgColor from '../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => (
  <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

const navConfig = [
  {
    title: 'Tổng Quan',
    path: '/',
    icon: icon('ic_analytics'),
  },
  {
    title: 'Người Dùng ',
    path: '/user',
    icon: icon('ic_user'),
  },
  {
    title: 'Tin Tức',
    path: '/blog',
    icon: icon('ic_blog'),
  },
  {
    title: 'Danh Mục',
    path: '/category',
    icon: icon('category-list-svgrepo-com'),
  },
  {
    title: 'Sản Phẩm',
    path: '/products',
    icon: icon('ic_cart'),
  },
  {
    title: 'Thanh Toán',
    path: '/payment',
    icon: icon('payment-methods-svgrepo-com'),
  },
  // {
  //   title: 'Not found',
  //   path: '/404',
  //   icon: icon('ic_disabled'),
  // },
];

export default navConfig;
