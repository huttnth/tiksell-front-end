import { Helmet } from 'react-helmet-async';
import AddCategoryPage from '../sections/AddCategory/AddCategoryPage';
// ----------------------------------------------------------------------

export default function BlogPage() {
  
  return (
    <>
      <Helmet>
        <title> Add-Category | TikSell </title>
      </Helmet>

     <AddCategoryPage></AddCategoryPage>
    </>
  );
}
