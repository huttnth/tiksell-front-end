import React, { useState, useEffect } from "react";
import { Table, Tag } from "antd";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import EditIcon from "@mui/icons-material/Edit";
import Iconify from "../components/iconify/iconify";
import DeleteIcon from "@mui/icons-material/Delete";
import { Modal } from "antd";
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import Ckeditorr from "../components/ckeditor/CategoryCkeditor";
import { useNavigate } from "react-router-dom";
import {
  getallCategory,
  editCategory,
  deleteCategory,
  createCategory,
} from "../../helpers/adminapi";
import { message } from "antd";

function Category() {
  const [name, setName] = useState("");
  const [keyword, setKeyword] = useState("");
  const [description, setDescription] = useState("");
  const [data, setData] = useState([]);
  const fetchData = async () => {
    try {
      const response = await getallCategory();

      if (Array.isArray(response.data?.data)) {
        const ketqua = response.data.data.filter(value => value.status !== 0);
        setData(ketqua);
      }
      
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [checkk, setCheckk] = useState(false);

  const showModalAdd = () => {
    setName("");
    setKeyword("");
    setDescription("");
    setCheckk(true);
    setIsModalOpen(true);
  };
  const handleOk = async () => {
    try {
      if (checkk) {
        const response = await createCategory({
          description: description,
          name: name,
          keyword: keyword,
        });

        if (response.status === 200) {
          message.success("Tạo mới thành công", 2);
          fetchData();
        } else {
          message.error("Có lỗi xảy ra khi tạo mới.");
        }
      } else {
        const response = await editCategory({
          description: description,
          name: name,
          keyword: keyword,
          id: idcheck,
        });

        if (response.status === 200) {
          message.success("Sửa thành công");
          fetchData();
        } else {
          message.error("Có lỗi xảy ra khi sửa.");
        }
      }
      setIsModalOpen(false);
    } catch (error) {
      message.error("Có lỗi xảy ra khi gửi yêu cầu.");
    }
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const [idcheck, setIdcheck] = useState("");
  const [isFullDescriptionVisible, setIsFullDescriptionVisible] =
    useState(false);

  const toggleDescriptionVisibility = () => {
    setIsFullDescriptionVisible(!isFullDescriptionVisible);
  };

  const editCategoryy = (i) => {
    setName(i.name);
    setKeyword(i.keyword);
    setDescription(i.description);
    setIdcheck(i.id);
    setIsModalOpen(true);
    setCheckk(false);
  };

  const deletee = async (id) => {
    try {
      const data = await deleteCategory({ id: id.id });
      if (data.status === 200) {
        message.success("xoá thành công");
        fetchData();
      }
    } catch (error) {
      message.error(error);
    }
  };

  const columns = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Tên",
      dataIndex: "name",
      key: "name",
      width: 250,
      render: (text) => (
        <p
          style={{
            maxWidth: "250px",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {text}
        </p>
      ),
    },
    {
      title: "Từ Khoá",
      dataIndex: "keyword",
      key: "keyword",
      width: 250,
      render: (text) => (
        <p
          style={{
            maxWidth: "250px",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {text}
        </p>
      ),
    },
    {
      title: "Mô Tả",
      key: "description",
      dataIndex: "description",
      width: 400,
      render: (text) => (
        <div
          dangerouslySetInnerHTML={{ __html: text.replace(/"/g, "") }}
          style={{
            maxWidth: "400px",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        ></div>
      ),
    },
    {
      title: "Hành Động",
      align: "center",
      key: "action",
      render: (id, row) => (
        <div className="flex justify-center">
          <button
            onClick={() => editCategoryy(id)}
            className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
          >
            <EditIcon style={{ color: "red" }} />
          </button>
          <button
            onClick={() => deletee(id)}
            className="flex gap-5 items-center py-3 px-7 hover:bg-[#919eab14]"
          >
            <DeleteIcon />
          </button>
        </div>
      ),
    },
  ];

  const nav = useNavigate();
  const [check, setCheck] = useState(false);

  const check_icon = () => {
    setCheck(true);
  };

  return (
    <div>
      <Paper>
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            pl: { sm: 2 },
            pr: { xs: 1, sm: 1 },
          }}
        >
          <Typography variant="h4" id="tableTitle" component="div">
            Danh Mục
          </Typography>
          <Button
            variant="contained"
            onClick={showModalAdd}
            color="inherit"
            startIcon={<Iconify icon="eva:plus-fill" />}
          >
            Thêm Danh Mục
          </Button>
          <Modal
            title={checkk ? "Thêm Danh Mục" : "Sửa Danh Mục"}
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
            okButtonProps={{
              style: { backgroundColor: "green", color: "white" },
            }}
            cancelButtonProps={{
              style: { backgroundColor: "red", color: "white" },
            }}
          >
            <TextField
              label="Name"
              name="title"
              value={name}
              onChange={(e) => setName(e.target.value)}
              style={{ width: "100%", marginBottom: "20px", marginTop: "20px" }}
            />
            <TextField
              label="Keyword"
              name="keyword"
              style={{ width: "100%", marginBottom: "20px" }}
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
            <Ckeditorr
              description={description}
              setDescription={setDescription}
            ></Ckeditorr>
          </Modal>
        </Toolbar>
        <Table columns={columns} dataSource={data} />
      </Paper>
    </div>
  );
}

export default Category;
